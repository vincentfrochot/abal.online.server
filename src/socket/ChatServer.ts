export class ChatServer {
  socket: SocketIO.Socket | null;

  constructor() {
    this.socket = null;
  }

  public listen(socket: SocketIO.Socket) {
    this.socket = socket;
    this.socket.on("message_push", (content: string) => {
      console.log(content);
      this.sendMessage(content);
    });
  }

  private sendMessage(content: string) {
    const data = {
      
    };
    this.socket?.broadcast.emit("message_pushed", data);
  }
}
