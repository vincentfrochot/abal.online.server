import * as socketio from "socket.io";
import logger from "../util/logger";

import { Game } from "../model/socket/Game";
import { User } from "../model/socket/User";
import { Message } from "../model/socket/Message";
import { JSONMessage as JSONMessageClient } from "../model/socket/client/JSONMessage";
import { JSONAuth } from "../model/socket/client/JSONAuth";
import {
  JSONGame as JSONGameClient,
  GameType,
} from "../model/socket/client/JSONGame";
import { JSONMatch } from "../model/socket/server/JSONMatch";
import { JSONMessage } from "../model/socket/server/JSONMessage";
import { JSONUser } from "../model/socket/server/JSONUser";
import { JSONGame as JSONGameServer } from "../model/socket/server/JSONGame";

import { User as UserDB } from "../model/mongo/user/User";
import { Match as MatchDB } from "../model/mongo/abalone/Match";

/*
@TODO:
- LOGOUT
- empêcher la connexion "multiple" (= depuis plusieurs onglets) si c'est trop source de problèmes
- ne pas pouvoir rejoindre sa propre partie si c'est source de problèmes
- lister les joueurs d'une partie sur la gameCard, sous forme de Chip
- faire fonctionner le mode replay
- enregistrer les parties jouées

all messages are of the following shape :
{
	"content": {
		"<resourceName>": {
      ...
    }
	}
}

*/

/*
  users ACL:

  guest : disable buttons client side, do nothing if user is not authenticated server side.
  {
    chat: {
      read: true,
      create: false
    },
    match: {
      read: true,
      create: false
    },
    analysis: {
      read: true,
      create: true // => for /play/load/<position>
    }
  }

  default authenticated user
  {
    chat: {
      read: true,
      create: true
    },
    match: {
      read: true,
      create: true
    },
    analysis: {
      read: true,
      create: true // => for /play/load/<position>
    }
  }
*/

export class RealtimeServer {
  // OnlineGame: { scores[], players[nicknameP1, nicknameP2], variant, startTimer }

  static MAX_MESSAGES = 19;

  static EVENTS = {
    // minimalist events system for alpha version
    client: {
      chat: {
        chat_init: "chat_init",
        message_push: "message_push", // { message }
      },
      user: {
        users_init: "users_init",
        user_log_in: "user_log_in", // at this point, call to API was sucessful and data was retrieved : { nickname, experience }
        user_log_out: "user_log_out", // { }
      },
      match: {
        matches_init: "matches_init",
      },
      game: {
        games_init: "games_init",
        game_create: "game_create", // { scores[], time, variant, ownerStarts... }
        game_analyze: "game_analyze",
        game_challenge: "game_challenge", // { gameId }
        game_cancel_challenge: "game_cancel_challenge",
        game_watch: "game_watch",
        game_start: "game_start", // { playerId }
        game_move_play: "game_move_play", // { gameId, move: Move }
        game_move_undo: "game_move_undo",
        analysis_move_play: "analysis_move_play",
        game_win_time: "game_win_time",
        game_resign: "game_resign", // { gameId }
        game_quit: "game_quit",
      },
    },
    server: {
      chat: {
        chat_init: "chat_init",
        message_pushed: "message_pushed", // broadcast { nickname, message, time }
        message_pushed_sound: "message_pushed_sound",
      },
      user: {
        number_of_users: "number_of_users",
        users_init: "users_init",
        user_joined: "user_joined",
        user_joined_you_users: "user_joined_you_users",
        user_joined_you_matches: "user_joined_you_matches",
        user_logged_in_users: "user_logged_in_users", // broadcast { users[] }
        user_logged_in_games: "user_logged_in_games", // broadcast { games[] }
        user_logged_in_you: "user_logged_in_you", // socket { acl,  }
        user_logged_out_users: "user_logged_out_users", // broadcast { users[] }
        user_logged_out_matches: "user_logged_out_matches", // broadcast { matches[] }
        user_logged_out_you: "user_logged_out_you", // give back the user a "guest-WXYZ" nickname
        user_disconnected_users: "user_disconnected_users",
        user_disconnected_matches: "user_disconnected_matches",
      },
      match: {
        // list of matchs being played
        matches_init: "matches_init",
        match_created: "match_created", // broadcast
        match_started_match: "match_started_match",
        match_started_users: "match_started_users",
        match_move_played: "match_move_played",
        match_won_match: "match_won_match",
        match_won_users: "match_won_users",
        match_quited: "match_quited", // broadcast
        match_unwatch: "match_unwatch", // send the list of matches
      },
      game: {
        // real games to display
        games_init: "games_init",
        game_created: "game_created", // socket { scores[], time, variant, ownerStarts... }
        game_created_sound: "game_created_sound",
        online_game_created: "online_game_created", // broadcast { onlineGames[] }
        game_challenged: "game_challenged", // broadcast { onlineGames }, notify each player linked to the game { playerId added to myGames  } => need to keep the association "gameId => playerId"  : socket.join(`games-${gameId}`): keep these in the class ???
        game_challenged_you: "game_challenged_you",
        game_challenged_owner: "game_challenged_owner",
        game_new_challenger_sound: "game_new_challenger_sound",
        game_canceled_challenge: "game_canceled_challenge",
        game_canceled_challenge_you: "game_canceled_challenge_you",
        game_canceled_challenge_owner: "game_canceled_challenge_owner",
        // a game could have a "socket room ID" as property to easily "broadcast to people concerned".
        game_watched: "game_watched",
        game_started: "game_started", // { playerId }
        game_started_sound: "game_started_sound",
        game_move_played: "game_move_played", // to each user associated to the game, { timer, boardHistory }
        game_move_played_sound: "game_move_played_sound",
        game_move_undone: "game_move_undone",
        game_won: "game_won",
        analysis_move_played: "analysis_move_played",
        game_quited: "game_quited",
        games_to_end: "games_to_end",
        game_doesnt_exist: "game_doesnt_exist", // { gameId } client need to remove this game from his local memory
        // game_ended: "game_ended" { reason: string }
      },
    },
  };

  io: SocketIO.Socket;
  chatMessages: Message[]; // last 50 chat messages
  chatMessageIndex: number;
  users: User[]; // real users
  games: Map<string, Game>; // the real games with board positions etc. the key of the map is the id of the game <=

  // these are used to easily find games related to authenticated users after they had a disconnection.
  // they are kept in server memory so authenticated players can receive games back even after a disconnection.
  // it's like a quick access in exhange of a bit of time maintaining it
  gamesPerOwner: Map<string, string[]>; // association between a player nickname and the game ids he created. Used to backup games that were pending a challenger.
  gamesPerPlayer: Map<string, string[]>; // association between a player nickname and the game ids he is playing in. Used to backup games being played.

  // @TODO: make sure sockets is useless since a socket id is enough for any operation we need.
  sockets: SocketIO.Socket[];

  constructor(io: SocketIO.Socket) {
    this.io = io;
    this.users = []; // all users (for onlineUserList)
    /*
      just an array of users
    */

    this.chatMessages = new Array();
    this.chatMessageIndex = 0;
    this.games = new Map();
    /*
      gameId: => Game
    */

    this.gamesPerOwner = new Map();
    this.gamesPerPlayer = new Map();
    /*
      e.g. if users <nickname1> and <nickname3>, and <nickname2> and <nickname3> are playing a match together
      nickname1: [
        0 => "" + gameId0,
      ],
      nickname2: [
        0 => "" + gameId1
      ],
      nickname3: [
        0 => "" + gameId0,
        1 => "" + gameId1
      ]
    */

    this.sockets = [];
  }

  // @TODO: implement server-side moves verification
  public start() {
    this.io.on("connection", (socket: socketio.Socket) => {
      const user = this.handleNewConnection(socket);

      socket.on("pingpong", (data) => {
        this.users.map((u) => {
          if (u.getSocketId() === socket.id) {
            u.setPing(data);
          }
          return u;
        });

        logger.log({
          level: "debug",
          message: `event: ${
            RealtimeServer.EVENTS.server.user.users_init
          } (ping), user: "${user.getNickname()}", ip: "${user.getIP()}"`,
        });
        socket.emit(RealtimeServer.EVENTS.server.user.users_init, {
          content: {
            users: this.generateUsersJSON(),
          },
        });
      });

      socket.on("reset_chat", () => {
        const user = this.getUserBySocket(socket);
        if (user && user.getNickname() === "vincent") {
          this.chatMessages = new Array();
          this.chatMessageIndex = 0;
          this.io.emit("reset_chat");
        }
      });

      socket.on(RealtimeServer.EVENTS.client.user.users_init, () => {
        this.handleUserInit(socket, user);
      });

      socket.on(RealtimeServer.EVENTS.client.chat.chat_init, () => {
        this.handleChatInit(socket, user);
      });

      socket.on(RealtimeServer.EVENTS.client.match.matches_init, () => {
        this.handleMatchesInit(socket, user);
      });

      socket.on(RealtimeServer.EVENTS.client.game.games_init, () => {
        this.handleGamesInit(socket, user);
      });

      // if user authenticates...
      socket.on(
        RealtimeServer.EVENTS.client.user.user_log_in,
        (data: JSONAuth) => {
          console.log(data);
          this.handleAuthentication(socket, data);
        }
      );

      socket.on(
        RealtimeServer.EVENTS.client.chat.message_push,
        (message: JSONMessageClient) => {
          this.handleNewMessage(message, socket);
        }
      );

      socket.on(
        RealtimeServer.EVENTS.client.game.game_create,
        (data: JSONGameClient) => {
          this.handleNewGame(socket, data);
        }
      );

      socket.on(
        RealtimeServer.EVENTS.client.game.game_analyze,
        (data: { gameId: number }) => {
          this.handleNewAnalysis(socket, data);
        }
      );

      socket.on(
        RealtimeServer.EVENTS.client.game.game_challenge,
        (data: { id: number }) => {
          this.handleGameNewChallenger(socket, data);
        }
      );

      socket.on(
        RealtimeServer.EVENTS.client.game.game_cancel_challenge,
        (data: { gameId: number }) => {
          this.handleGameCancelChallenge(socket, data);
        }
      );

      socket.on(
        RealtimeServer.EVENTS.client.game.game_watch,
        (data: { id: number }) => {
          this.handleGameWatch(socket, data);
        }
      );

      socket.on(
        RealtimeServer.EVENTS.client.game.game_start,
        (data: { id: number; nickname: string }) => {
          this.handleGameStart(socket, data);
        }
      );

      socket.on(
        RealtimeServer.EVENTS.client.game.game_move_play,
        (
          data: {
            gameId: number;
            computedMove: {
              move: { newCoordinates1D: number[]; oldCoordinates1D: number[] };
              vector: { arrowPositions: number[]; direction: number };
            };
            pushed: boolean;
          } /*JSONMoveClient*/
        ) => {
          this.handleMove(data);
        }
      );

      socket.on(
        RealtimeServer.EVENTS.client.game.game_move_undo,
        (
          data: {
            gameId: number;
          } /*JSONMoveClient*/
        ) => {
          this.handleUndo(data, socket);
        }
      );

      socket.on(
        RealtimeServer.EVENTS.client.game.analysis_move_play,
        (
          data: {
            gameId: number;
            computedMove: {
              move: { newCoordinates1D: number[]; oldCoordinates1D: number[] };
              vector: { arrowPositions: number[]; direction: number };
            };
            pushed: boolean;
            index: number;
          } /*JSONMoveClient*/
        ) => {
          this.handleAnalysisMove(data, socket);
        }
      );

      socket.on(
        RealtimeServer.EVENTS.client.game.game_win_time,
        (data: { gameId: string }) => {
          this.handleWinByTime(data, socket);
        }
      );

      socket.on(
        RealtimeServer.EVENTS.client.game.game_quit,
        (data: { gameId: string }) => {
          this.handleGameQuit(data, socket);
        }
      );

      // when a user log out, all the games he was playing are closed for his opponent and for watchers : they receive a list of gameIds to switch to analysis mode :
      // ... server won't maintain in memory the list of past games, since we now have the "replay" page.
      // the user who logged out receives his new user informations (which is like a reset to Guest default values)
      // everybody has to update their list of users and matches.
      socket.on(RealtimeServer.EVENTS.client.user.user_log_out, () => {
        // click on "logout" button in the header

        const user = this.users.find((u) => u.getSocketId() === socket.id);
        if (!user) {
          logger.error(
            `user_log_out: user with socketId "${socket.id}" tried to log out but he was not found in the list of users.`
          );
          return;
        }
        logger.info(`user_log_out: user "${user.getNickname()}" logged out.`);

        // delete pending and finished games the user owned
        this.gamesPerOwner.get(user.getNickname())?.forEach((gameId) => {
          const game = this.games.get("" + gameId);
          if (game) {
            if (!game.isStarted()) {
              logger.info(
                `user_log_out: Deleting a non started game "${game.getId()}" for user "${user.getNickname()}".`
              );
              this.games.delete("" + game.getId());
            } else if (game.isFinished()) {
              logger.info(
                `user_log_out: Deleting a finished game "${game.getId()}" for user "${user.getNickname()}".`
              );
              this.games.delete("" + game.getId());
            } else if (game.getType() === GameType.analysis) {
              logger.info(
                `user_log_out: Deleting an analysis game "${game.getId()}" for user "${user.getNickname()}".`
              );
              this.games.delete("" + game.getId());
            }
          } else {
            logger.error(
              `user_log_out: gamesPerOwner possess a game not found in games list.`
            );
          }
        });
        logger.info(
          `user_log_out: Deleting gamesPerOwner for user "${user.getNickname()}".`
        );
        this.gamesPerOwner.delete(user.getNickname());

        // now considering the active games
        // make the leaving user lose all the games he was playing
        const winnerByForfeit: Map<string, User> = new Map(); // Map are used here because playing multiple games in the same time with the same person is possible
        const winnerPerGameId: Map<string, User> = new Map();
        const watchers: Map<string, User> = new Map();
        const gamesToEnd: Game[] = [];
        this.gamesPerPlayer.get(user.getNickname())?.forEach((gameId) => {
          logger.info(
            `user_log_out: Considering game ID "${gameId}" using gamesPerPlayer.`
          );
          const game = this.games.get("" + gameId);
          if (game) {
            if (game.isStarted() && !game.isFinished()) {
              logger.info(`user_log_out: game ID "${gameId}" has to be ended.`);

              gamesToEnd.push(game);
              const opponent = game.getOpponentFromMyNickname(
                user.getNickname()
              );
              if (!opponent) {
                logger.error(`user_log_out: opponent not found.`);
              } else {
                winnerByForfeit.set("" + opponent.getNickname(), opponent);
                winnerPerGameId.set("" + game.getId(), opponent);
              }

              const gameWatchers = game.getWatchers();
              gameWatchers.map((w) => {
                if (w) {
                  watchers.set("" + w.getNickname(), w);
                } else {
                  logger.error(`user_log_out: watcher not found.`);
                }
              });
            }
          } else {
            logger.error(
              `user_log_out: gamePerPlayer possess a game not found in games list.`
            );
          }
        });

        // now we have the map of all winners and watchers and the list of games to end, we can update the games and send signals :
        // - players and watchers need to receive this.games.
        // - everybody need to update their list of users and matches
        gamesToEnd.map((g) => {
          logger.info(
            `user_log_out: Considering game ID "${g.getId()}" using gamesToEnd.`
          );
          g.finish(winnerPerGameId.get("" + g.getId()) || null);
          // if (g.getHistoScores().length > 4) {
          const players = g.getPlayers();
          if (!players[0] || !players[1]) {
            logger.error(
              `user_log_out: at least one of the players cannot be retrieved using game.getPlayers()`
            );
          } else {
            const winnerByForfeit =
              players[0].getNickname() === user.getNickname()
                ? players[1]
                : players[0];
            const loserByForfeit =
              players[0].getNickname() === user.getNickname()
                ? players[0]
                : players[1];
            loserByForfeit.setExperience(loserByForfeit.getExperience() + 1);
            winnerByForfeit.setExperience(winnerByForfeit.getExperience() + 1);
            this.updateUserInDB(loserByForfeit);
            this.updateUserInDB(winnerByForfeit); // @TODO: careful, loserByForfeit nickname is probably not available anymore when we enter the updateUserInDB method. idea to fix: use a nickname const before logging out the user.

            const gamesOfWinner: string[] = [];
            this.gamesPerPlayer
              .get("" + winnerByForfeit.getNickname())
              ?.forEach((gameId) => {
                if ("" + gameId !== "" + g.getId()) {
                  gamesOfWinner.push(gameId);
                }
              });
            if (gamesOfWinner.length === 0) {
              // @@TODO: fix this: this is probably not working if they were playing multiple games together
              logger.info(
                `winner "${winnerByForfeit.getNickname()}" of game "${g.getId()}" was not playing any other game.`
              );
              winnerByForfeit.setPlaying(false);
            }
            this.gamesPerPlayer.set(
              "" + winnerByForfeit.getNickname(),
              gamesOfWinner
            );

            this.saveGameInDB(g);
          }
          // }
          this.games.delete("" + g.getId());
        });

        this.gamesPerPlayer.delete(user.getNickname());
        user.logout();

        watchers.forEach((w) => {
          if (w) {
            this.io
              .to(w.getSocketId())
              .emit(RealtimeServer.EVENTS.server.game.games_to_end, {
                content: {
                  games: gamesToEnd,
                },
              });
          }
        });

        winnerByForfeit.forEach((w) => {
          if (w) {
            logger.info(
              `sending gamesToEnd to user whose nickname is "${w.getNickname()}"`
            );
            this.io
              .to(w.getSocketId())
              .emit(RealtimeServer.EVENTS.server.game.games_to_end, {
                content: {
                  games: gamesToEnd,
                },
              });
          }
        });

        // update onlineMatchList and onlinePlayerList for everybody
        this.sendDataToClient(
          "io",
          RealtimeServer.EVENTS.server.user.user_logged_out_users,
          {
            users: this.generateUsersJSON(),
          }
        );
        this.sendDataToClient(
          "io",
          RealtimeServer.EVENTS.server.user.user_logged_out_matches,
          {
            matches: this.generateMatchesFromGames(),
          }
        );
        // user who logged out has to clean his games, update his infos and receive the update list of online matches
        this.sendDataToClient(
          // close his active games
          "socket",
          RealtimeServer.EVENTS.server.user.user_logged_out_you,
          {
            user: user.toJSON(),
          },
          socket
        );
      });

      socket.on("disconnect", () => {
        // removing analysis games
        this.removeAnalysisGamesFromUser(socket);

        // @TODO later: remove player from games he is watching.

        // remove games pending challenger
        const gameOwner = this.getUserBySocket(socket);
        if (!gameOwner) {
          logger.error(
            `user with socketId "${socket.id}" was not found in this.users`
          );
        } else {
          logger.warn(
            `user "${gameOwner.getNickname()}" is being disconnected.`
          );
          const gameIdsToKeep: string[] = [];
          this.gamesPerOwner
            .get("" + gameOwner.getNickname())
            ?.forEach((gameId) => {
              const game = this.games.get(gameId);
              if (!game) {
                logger.error(
                  `disconnect(): game id "${gameId}" does not exist in this.games`
                );
              } else {
                if (!game.isStarted()) {
                  this.games.delete(gameId);
                  logger.info(
                    `disconnect(): deleting game "${gameId}". It was not started.`
                  );
                } else {
                  gameIdsToKeep.push("" + game.getId());
                }
              }
            });
          this.gamesPerOwner.set("" + gameOwner.getNickname(), gameIdsToKeep);

          // deleting finished games he created
          const finishedGamesToDelete: string[] = [];
          this.games.forEach((g) => {
            if (g.isFinished()) {
              if (g.getOwner().getNickname() === gameOwner.getNickname()) {
                finishedGamesToDelete.push("" + g.getId());
              }
            }
          });
          finishedGamesToDelete.forEach((g) => {
            if (this.games.delete(g)) {
              logger.info(
                `disconnect(): deleting game "${g}". It was finished.`
              );
            } else {
              logger.error(
                `disconnect(): could not delete game "${g}" altough it was finished.`
              );
            }
          });
        }

        // else if (game.isFinished()) {
        //   this.games.delete(gameId);
        //   logger.info(`disconnect(): deleting game "${gameId}". It was finished.`);

        // @TODO: ne pas freeze le temps de la partie, afin qu'il perde au temps s'il ne se reconnecte pas assez vite.
        // this.removeGamesFromUser(socket);
        this.removeUser(socket);
        this.sendDataToClient(
          "io",
          RealtimeServer.EVENTS.server.user.user_disconnected_users,
          {
            users: this.generateUsersJSON(),
          }
        );
        this.sendDataToClient(
          "io",
          RealtimeServer.EVENTS.server.user.user_disconnected_matches,
          {
            matches: this.generateMatchesFromGames(),
          }
        );
        this.sendDataToClient(
          "io",
          RealtimeServer.EVENTS.server.user.number_of_users,
          {
            cardinal: this.users.length,
          }
        );
      });
    });
  }

  /* CHAT */
  private handleNewMessage = (
    JSONMessage: JSONMessageClient,
    socket: socketio.Socket
  ) => {
    const user = this.getUserBySocket(socket);
    if (!user) {
      return;
    }
    const acl = user.getACL().toJSON();

    if (!acl.chatWrite) {
      logger.log({
        level: "debug",
        message: `Message blocked from "${user.nickname}": "${JSONMessage.value}"`,
      });
      return;
    }
    const message = new Message(user.nickname, JSONMessage.value);
    logger.log({
      level: "info",
      message: `Message from "${user.nickname}":`,
    });

    if (this.chatMessageIndex > RealtimeServer.MAX_MESSAGES) {
      this.chatMessages.shift();
    } else {
      this.chatMessageIndex++;
    }
    this.chatMessages.push(message);
    console.log(
      'new message from "' +
        user.getNickname() +
        '": "' +
        JSONMessage.value +
        '"'
    );
    this.sendDataToClient(
      "io",
      RealtimeServer.EVENTS.server.chat.message_pushed,
      {
        message: message.toJSON(),
      }
    );
    this.sendDataToClient(
      "io",
      RealtimeServer.EVENTS.server.chat.message_pushed_sound,
      {}
    );
  };

  private generateMessages = (): JSONMessage[] => {
    return this.chatMessages.filter((e) => e !== null).map((e) => e.toJSON());
  };

  /* USER */
  private handleNewConnection = (socket: socketio.Socket): User => {
    // add new user to our list of onlineUsers
    const user = this.addUser(socket);

    // new user has to know:
    // - the last K messages that were sent
    // - his nickname (his socketId since he is not authenticated yet)
    // - the list of online users
    // - the list of matches
    this.sendDataToClient(
      "socket",
      RealtimeServer.EVENTS.server.user.user_joined_you_users,
      {
        users: this.generateUsersJSON(),
      },
      socket
    );
    this.sendDataToClient(
      "socket",
      RealtimeServer.EVENTS.server.user.user_joined_you_matches,
      {
        matches: this.generateMatchesFromGames(),
      },
      socket
    );
    this.sendDataToClient(
      "socket",
      RealtimeServer.EVENTS.server.chat.chat_init,
      {
        messages: this.generateMessages(),
      },
      socket
    );

    // all people have to update their users list
    this.sendDataToClient("io", RealtimeServer.EVENTS.server.user.user_joined, {
      users: this.generateUsersJSON(),
    });

    this.sendDataToClient(
      "io",
      RealtimeServer.EVENTS.server.user.number_of_users,
      {
        cardinal: this.users.length,
      }
    );

    return user;
  };

  private handleUserInit = (socket: socketio.Socket, user: User) => {
    logger.log({
      level: "debug",
      message: `event: ${
        RealtimeServer.EVENTS.server.user.users_init
      }, user: "${user.getNickname()}"`,
    });
    socket.emit(RealtimeServer.EVENTS.server.user.users_init, {
      content: {
        users: this.generateUsersJSON(),
      },
    });
  };

  private handleChatInit = (socket: socketio.Socket, user: User) => {
    logger.log({
      level: "debug",
      message: `event: ${
        RealtimeServer.EVENTS.server.chat.chat_init
      }, user: "${user.getNickname()}"`,
    });
    socket.emit(RealtimeServer.EVENTS.server.chat.chat_init, {
      content: {
        messages: this.generateMessages(),
      },
    });
  };

  private handleMatchesInit = (socket: socketio.Socket, user: User) => {
    logger.log({
      level: "debug",
      message: `event: ${
        RealtimeServer.EVENTS.server.match.matches_init
      }, user: "${user.getNickname()}"`,
    });
    socket.emit(RealtimeServer.EVENTS.server.match.matches_init, {
      content: {
        matches: this.generateMatchesFromGames(),
      },
    });
  };

  private handleGamesInit = (socket: socketio.Socket, user: User) => {
    /*    const user = this.getUserBySocket(socket);
    if (!user) {
      return;
    }
*/
    if (!user.isAuthenticated) {
      logger.warn(
        `Tried to retrieve games but user ${user.getNickname()} is not authenticated.`
      );
      return;
    }

    const JSONGamesOfUser = this.getJSONGamesOfUser(user.getNickname());

    logger.log({
      level: "debug",
      message: `event: ${
        RealtimeServer.EVENTS.server.game.games_init
      }, user: "${user.getNickname()}"`,
    });
    socket.emit(RealtimeServer.EVENTS.server.game.games_init, {
      content: {
        games: JSONGamesOfUser,
      },
    });
  };

  private handleAuthentication = (socket: socketio.Socket, data: JSONAuth) => {
    this.authenticate(socket, data);
    this.users.sort((a, b) => {
      if (a.isAuthenticated() === false) {
        return 1;
      }
      if (b.isAuthenticated() === false) {
        return -1;
      }
      if (a.getNickname() > b.getNickname()) {
        return 1;
      } else {
        return -1;
      }
    });

    this.sendDataToClient(
      "socket",
      RealtimeServer.EVENTS.server.user.user_logged_in_users,
      {
        users: this.generateUsersJSON(),
      },
      socket
    );

    // send back any game they were in.
    if (!data.nickname) {
      logger.warn(`handleAuthentication(): anonymous user loaded the page.`);
      return;
    }
    const games = this.getJSONGamesOfUser(data.nickname);

    this.sendDataToClient(
      "socket",
      RealtimeServer.EVENTS.server.user.user_logged_in_games,
      {
        games: games,
      },
      socket
    );

    // send the ACL to the newly authenticated user
    /*    const acl = 
    this.sendDataToClient(
      "socket",
      RealtimeServer.EVENTS.server.user.user_logged_in_acl,
      {
        acl: acl,
      },
      socket
    );*/
  };

  private getJSONGamesOfUser = (nickname: string): JSONGameServer[] => {
    const games: JSONGameServer[] = [];
    this.gamesPerOwner.get(nickname)?.forEach((gameId) => {
      const game = this.games.get("" + gameId);
      if (game && !game?.isStarted()) {
        games.push(game.toJSON());
      }
    });
    this.gamesPerPlayer.get(nickname)?.forEach((gameId) => {
      const game = this.games.get("" + gameId);
      if (game) {
        games.push(game.toJSON());
      }
    });
    return games;
  };

  private addUser = (socket: socketio.Socket): User => {
    const user = new User(socket);
    user.log("Connection", "info");
    console.log('new user: "' + user.nickname + '"');
    this.users = [...this.users, user];
    return user;
  };

  /* GAME */
  private handleNewGame = (socket: socketio.Socket, data: JSONGameClient) => {
    const owner = this.getUserBySocket(socket);
    if (!owner) {
      return;
    }

    if (!owner.isAuthenticated) {
      logger.error(
        `game was about to be created but user ${owner.getNickname()} is not authenticated.`
      );
      return;
    }

    if (data.type && data.type === "analysis") {
      const acl = owner.getACL();
/*
      if (owner.getNickname() !== "vincent") {
        logger.error(
          `user "${owner.getNickname()}" is not authorized to create analysis game.`
        );
        return;
      }
      */

      /*      if (!acl.createAnalysis) {
        logger.error(
          `user "${owner.getNickname()}" is not authorized to create analysis game.`
        );
        return;
      }
      */
    }


    const game = new Game(owner, data);
    this.games.set("" + game.getId(), game);
    const gamesFromUser = this.gamesPerOwner.get(owner.getNickname()) || [];
    this.gamesPerOwner.set(owner.getNickname(), [
      ...gamesFromUser,
      "" + game.getId(),
    ]);
    console.log(
      `new game "${game.getType()}" in variant "${
        game.getVariant().name
      }" from "${owner.getNickname()}"`
    );
    if (game.getVariant().name === "custom") {
      const positions = game.getHistoPositions();
      console.log(`positions : ${positions[0]}`);
    }

    this.sendDataToClient(
      "io",
      RealtimeServer.EVENTS.server.match.match_created,
      { matches: this.generateMatchesFromGames() }
    );
    this.sendDataToClient(
      "socket",
      RealtimeServer.EVENTS.server.game.game_created,
      {
        game: game.toJSON(),
      },
      socket
    );
    this.sendDataToClient(
      "io",
      RealtimeServer.EVENTS.server.game.game_created_sound,
      {}
    );
  };

  private handleNewAnalysis = (
    socket: socketio.Socket,
    data: { gameId: number }
  ) => {
    const referenceGame = this.games.get("" + data.gameId);
    if (!referenceGame) {
      logger.error(`handleNewAnalysis(): game "${data.gameId}" was not found.`);
      return;
    }

    const owner = this.getUserBySocket(socket);
    if (!owner) {
      return;
    }

    const acl = owner.getACL();
    if (!acl.analysisCreate) {
      logger.error(
        `user "${owner.getNickname()}" is not authorized to create analysis game.`
      );
      return;
    }

    const analysisGame = Game.createAnalysis(owner, referenceGame);
    const gamesPerOwner = this.gamesPerOwner.get(owner.getNickname());
    gamesPerOwner?.push("" + analysisGame.getId());
    this.gamesPerOwner.set(owner.getNickname(), gamesPerOwner || []);
    this.games.set("" + analysisGame.getId(), analysisGame);

    this.sendDataToClient(
      "io",
      RealtimeServer.EVENTS.server.match.match_created,
      { matches: this.generateMatchesFromGames() }
    );
    this.sendDataToClient(
      "socket",
      RealtimeServer.EVENTS.server.game.game_created,
      {
        game: analysisGame.toJSON(),
      },
      socket
    );
  };

  private handleGameNewChallenger = (
    socket: socketio.Socket,
    data: { id: number }
  ) => {
    const game = this.games.get("" + data.id);

    if (!game) {
      logger.error(
        `handleGameNewChallenger(): game "${data.id}" was not found.`
      );
      return;
    }

    const newChallenger = this.getUserBySocket(socket);
    if (!newChallenger) {
      logger.error(
        `handleGameNewChallenger(): user with socketid "${socket.id}" was not found`
      );
      return;
    }

    if (newChallenger.getNickname() === game.getOwner().getNickname()) {
      logger.error(
        `handleGameNewChallenger(): user with same nickname "${newChallenger.getNickname()}" tried to join as challenger.`
      );
      return;
    }

    const challengers = game.getChallengers();

    // already in the challengers list ?
    if (
      challengers.find((u) => u?.getNickname() === newChallenger?.getNickname())
    ) {
      logger.warn(
        `handleGameNewChallenger(): user "${newChallenger.getNickname()}" is already a challenger.`
      );
      return;
    }
    const gameOwner = game.getOwner();
    game.addChallenger(newChallenger);
    this.games.set("" + game.getId(), game);

    logger.info(
      `new challenger "${newChallenger.getNickname()}" in game of "${gameOwner.getNickname()}"`
    );

    this.io
      .to(newChallenger.socketId)
      .emit(RealtimeServer.EVENTS.server.game.game_challenged_you, {
        content: {
          gameId: game.getId(),
        },
      });

    this.io
      .to(gameOwner.socketId)
      .emit(RealtimeServer.EVENTS.server.game.game_challenged, {
        content: {
          game: game.toJSON(),
        },
      });

    this.io
      .to(gameOwner.socketId)
      .emit(RealtimeServer.EVENTS.server.game.game_challenged_owner, {
        content: {
          gameId: game.getId(),
          newChallenger: newChallenger.getNickname(),
        },
      });
    // sounds
    this.io
      .to(newChallenger.socketId)
      .emit(RealtimeServer.EVENTS.server.game.game_new_challenger_sound, {});
    this.io
      .to(gameOwner.socketId)
      .emit(RealtimeServer.EVENTS.server.game.game_new_challenger_sound, {});
  };

  private handleGameCancelChallenge = (
    socket: socketio.Socket,
    data: { gameId: number }
  ) => {
    const game = this.games.get("" + data.gameId);

    if (!game) {
      logger.error(
        `handleGameCancelChallenge(): game "${data.gameId}" was not found.`
      );
      return;
    }

    const challengerToRemove = this.getUserBySocket(socket);
    if (!challengerToRemove) {
      logger.error(
        `handleGameCancelChallenge(): user with socketid "${socket.id}" was not found`
      );
      return;
    }

    const gameOwner = game.getOwner();
    game.removeChallenger(challengerToRemove);
    this.games.set("" + game.getId(), game);

    logger.info(
      `removed challenger "${challengerToRemove.getNickname()}" from game of "${gameOwner.getNickname()}"`
    );

    this.io
      .to(challengerToRemove.socketId)
      .emit(RealtimeServer.EVENTS.server.game.game_canceled_challenge_you, {
        content: {
          gameId: game.getId(),
        },
      });

    this.io
      .to(gameOwner.socketId)
      .emit(RealtimeServer.EVENTS.server.game.game_canceled_challenge, {
        content: {
          game: game.toJSON(),
        },
      });

    this.io
      .to(gameOwner.socketId)
      .emit(RealtimeServer.EVENTS.server.game.game_canceled_challenge_owner, {
        content: {
          gameId: game.getId(),
        },
      });
  };

  private handleGameWatch = (socket: socketio.Socket, data: { id: number }) => {
    const newWatcher = this.getUserBySocket(socket);
    if (!newWatcher) {
      return;
    }
    const game = this.addWatcherToGame("" + data.id, newWatcher);
    if (!game) {
      logger.error(`handleGameWatch(): game "${data.id}" not found.`);
      return;
    }

    // send game to new watcher
    this.sendDataToClient(
      "socket",
      RealtimeServer.EVENTS.server.game.game_watched,
      {
        game: game.toJSON(),
      },
      socket
    );

    // send updated game to players if they were still playing. @TODO: later on, just send the updated number of players instead, no need to send the whole game !!!
    if (!game.isFinished()) {
      game.getPlayers().forEach((e) => {
        if (e) {
          this.io
            .to(e.socketId)
            .emit(RealtimeServer.EVENTS.server.game.game_watched, {
              content: {
                game: game.toJSON(),
              },
            });
        }
      });
    }
  };

  private handleGameStart = (
    socket: socketio.Socket,
    data: { id: number; nickname: string }
  ) => {
    const game = this.games.get("" + data.id);
    if (!game) {
      logger.error(`handleGameStart(): game "${data.id}" not found.`);
      return;
    }

    const opponentIndex = this.users.findIndex(
      (u) => u.nickname === data.nickname
    );
    if (opponentIndex === -1) {
      let ownerNickname = "NOT FOUND";
      if (game.getOwner() && game.getOwner().getNickname()) {
        ownerNickname = game.getOwner().getNickname();
      }
      logger.error(
        `handleGameStart(): challenger of game of owner "${ownerNickname}" whose nickname is "${data.nickname}" was not found : he is probably disconnected.`
      );
      return;
    }
    this.users[opponentIndex].setPlaying(true);

    const ownerIndex = this.users.findIndex(
      (u) => u.nickname === game.getOwner().getNickname()
    );
    if (ownerIndex === -1) {
      logger.error(
        `handleGameStart(): owner whose nickname is "${game
          .getOwner()
          .getNickname()}" was not found.`
      ); // @TODO: typically, would be better to use the Map gamesPerOwner here, to make sure we can still access the nickname even if the game owner do not exist anymore so we handle properly the edge case.
      return;
    }
    this.users[ownerIndex].setPlaying(true);

    const gamesOwnerIsPlaying =
      this.gamesPerPlayer.get(game.getOwner().getNickname()) || [];
    this.gamesPerPlayer.set(game.getOwner().getNickname(), [
      ...gamesOwnerIsPlaying,
      "" + game.getId(),
    ]);
    const gamesOpponentIsPlaying = this.gamesPerPlayer.get(data.nickname) || [];
    this.gamesPerPlayer.set(data.nickname, [
      ...gamesOpponentIsPlaying,
      "" + game.getId(),
    ]);

    game.start(this.users[opponentIndex]);
    game.getPlayers().forEach((player) => {
      if (player) {
        logger.debug(`game_started sent to "${player.getNickname()}".`);
        this.io
          .to(player.socketId)
          .emit(RealtimeServer.EVENTS.server.game.game_started, {
            content: {
              game: game.toJSON(),
            },
          });
        this.io
          .to(player.socketId)
          .emit(RealtimeServer.EVENTS.server.game.game_started_sound, {
            content: {
              game: game.toJSON(),
            },
          });
      }
    });
    // const matchesJSON = this.generateMatchesFromGames();
    const usersJSON = this.generateUsersJSON();
    this.sendDataToClient(
      "io",
      RealtimeServer.EVENTS.server.match.match_started_match,
      {
        match: game.matchToJSON(),
      }
    );
    this.sendDataToClient(
      "io",
      RealtimeServer.EVENTS.server.match.match_started_users,
      {
        users: usersJSON,
      }
    );
  };

  private handleMove = (
    data: {
      gameId: number;
      computedMove: {
        move: { newCoordinates1D: number[]; oldCoordinates1D: number[] };
        vector: { arrowPositions: number[]; direction: number };
      };
      pushed: boolean;
    } /*JSONMoveClient*/
  ) => {
    const game = this.games.get("" + data.gameId);
    if (!game) {
      logger.error(`handleMove(): game "${data.gameId}" was not found.`);
      return;
    }

    if (game.isFinished()) {
      logger.error(`handleMove(): game "${data.gameId}" is already finished.`);
      return;
    }

    game.playMove(data);
    this.games.set("" + data.gameId, game);

    const owner = game.getOwner();
    this.io
      .to(owner.socketId)
      .emit(RealtimeServer.EVENTS.server.game.game_move_played, {
        content: {
          game: game.toJSON(),
        },
      });
    this.io
      .to(owner.socketId)
      .emit(RealtimeServer.EVENTS.server.game.game_move_played_sound);

    const watchers = game.getWatchers();
    watchers.forEach((watcher) => {
      if (watcher) {
        this.io
          .to(watcher.socketId)
          .emit(RealtimeServer.EVENTS.server.game.game_move_played, {
            content: {
              game: game.toJSON(),
            },
          });
      }
    });

    const opponent = game.getOpponent();
    // @TODO: make sure opponent timer is used if the opponent disconnects
    if (!opponent) {
      // if the opponent is not available anymore, the game state has to update still for us.
      logger.error(
        `handleMove(): opponent was not found in game "${game.getId()}".`
      );
      return;
    }

    if (game.isFinished()) {
      // update this.gamesPerPlayer and this.gamesPerOwner()
      // ... and check if players are playing another game.
      // if they are not, update their user property to playing=false
      this.updateGamesPerPlayerAndOwnerAfterGameFinishes(game, owner, opponent);

      owner.setExperience(owner.getExperience() + 1);
      opponent.setExperience(opponent.getExperience() + 1);
      this.updateUserInDB(owner);
      this.updateUserInDB(opponent);

      if (game.getHistoScores().length > 4) {
        this.saveGameInDB(game);
      }

      this.sendDataToClient(
        "io",
        RealtimeServer.EVENTS.server.match.match_won_match,
        {
          match: game.matchToJSON(),
        }
      );
      this.sendDataToClient(
        "io",
        RealtimeServer.EVENTS.server.match.match_won_users,
        {
          users: this.generateUsersJSON(),
        }
      );
    }

    logger.info(
      `move played in match between "${owner.getNickname()}" and "${opponent.getNickname()}"`
    );
    this.io.emit(RealtimeServer.EVENTS.server.match.match_move_played, {
      content: {
        match: game.matchToJSON(),
      },
    });
    this.io
      .to(opponent.socketId)
      .emit(RealtimeServer.EVENTS.server.game.game_move_played, {
        content: {
          game: game.toJSON(),
        },
      });
    this.io
      .to(opponent.socketId)
      .emit(RealtimeServer.EVENTS.server.game.game_move_played_sound);
  };

  private handleUndo = (
    data: {
      gameId: number;
    },
    socket: socketio.Socket
  ) => {
    const game = this.games.get("" + data.gameId);
    if (!game) {
      logger.error(`handleUndo(): game "${data.gameId}" was not found.`);
      return;
    }

    if (game.getType() !== GameType.analysis) {
      logger.error(
        `handleUndo(): game type is equal to "${game.getType}", not "${GameType.analysis}".`
      );
      return;
    }

    game.undoLastMove();
    this.games.set("" + data.gameId, game);
    this.io
      .to(socket.id)
      .emit(RealtimeServer.EVENTS.server.game.game_move_undone, {
        content: {
          game: game.toJSON(),
        },
      });
  };

  private handleAnalysisMove = (
    data: {
      gameId: number;
      computedMove: {
        move: { newCoordinates1D: number[]; oldCoordinates1D: number[] };
        vector: { arrowPositions: number[]; direction: number };
      };
      pushed: boolean;
      index: number;
    } /*JSONMoveClient*/,
    socket: socketio.Socket
  ) => {
    const game = this.games.get("" + data.gameId);
    if (!game) {
      logger.error(
        `handleAnalysisMove(): game "${data.gameId}" was not found.`
      );
      return;
    }

    game.playAnalysisMove(data);
    this.games.set("" + data.gameId, game);

    this.io
      .to(socket.id)
      .emit(RealtimeServer.EVENTS.server.game.game_move_played, {
        content: {
          game: game.toJSON(),
        },
      });

    game.getWatchers().forEach((watcher) => {
      if (watcher) {
        this.io
          .to(watcher.socketId)
          .emit(RealtimeServer.EVENTS.server.game.game_move_played, {
            content: {
              game: game.toJSON(),
            },
          });
      }
    });
  };

  private handleWinByTime = (
    data: { gameId: string },
    socket: socketio.Socket
  ) => {
    logger.info(`handleWinByTime(): checking game "${data.gameId}".`);
    const game = this.games.get("" + data.gameId);
    if (!game) {
      logger.error(`handleWinByTime(): could not find game "${data.gameId}"`);
      this.sendDataToClient(
        // we provide information to checker his game does not exist anymore.
        "socket",
        RealtimeServer.EVENTS.server.game.game_doesnt_exist,
        { gameId: data.gameId },
        socket
      );
      return;
    }

    if (game.isFinished()) {
      logger.error(
        `handleWinByTime(): game "${data.gameId}" is already finished.`
      );
      return;
    }

    if (!game.checkWinByTime()) {
      logger.info(
        `handleWinByTime(): game "${data.gameId}" is not yet won by time.`
      );
      return;
    }

    if (game.getHistoScores().length > 4) {
      this.saveGameInDB(game);
    } else {
      logger.info(
        `game "${game.getId()}" was not saved because less than 4 moves were played.`
      );
    }

    const gameOwner = game.getOwner();
    const opponent = game.getOpponent();
    if (!opponent) {
      logger.error(
        `handleWinByTime(): opponent of game "${data.gameId}" was not found.`
      );
      // return;
    }

    // @TODO: check its working properly : the goal is to clean the memory from past games games

    // game.getWatchers().forEach((watcher) => {
    //   if (watcher) {
    //     this.io
    //       .to(watcher.socketId)
    //       .emit(RealtimeServer.EVENTS.server.game.game_quited, {
    //         content: {
    //           id: game.getId(),
    //         },
    //       });
    //   }
    // });
    game.getPlayers().forEach((player) => {
      if (player) {
        this.io
          .to(player.socketId)
          .emit(RealtimeServer.EVENTS.server.game.game_won, {
            content: {
              game: game.toJSON(),
            },
          });
        player.setExperience(player.getExperience() + 1);
        this.updateUserInDB(player);
      }
    });

    /*
    if (this.games.delete("" + game.getId())) {
      // clean up delete game from list since we have a Replay page
      logger.info(`game "${game.getId()}" was deleted.`);
    }
    */
    this.updateGamesPerPlayerAndOwnerAfterGameFinishes(
      game,
      gameOwner,
      opponent
    );

    logger.info(`handleWinByTime(): game "${data.gameId}" was won by time.`);
    // game.getPlayers().forEach((player) => {
    //   if (player) {
    //     this.updateUserInDB(player);

    //     this.io
    //       .to(player.socketId)
    //       .emit(RealtimeServer.EVENTS.server.game.game_won, {
    //         content: {
    //           game: game.toJSON(),
    //         },
    //       });
    //   }
    // });

    game.getWatchers().forEach((watcher) => {
      if (watcher) {
        this.io
          .to(watcher.socketId)
          .emit(RealtimeServer.EVENTS.server.game.game_won, {
            content: {
              game: game.toJSON(),
            },
          });
      }
    });

    // remove the game from the list
    this.io.emit(RealtimeServer.EVENTS.server.match.match_won_match, {
      content: {
        match: game.matchToJSON(),
      },
    });

    this.io.emit(RealtimeServer.EVENTS.server.match.match_won_users, {
      content: {
        users: this.generateUsersJSON(),
      },
    });
  };

  private handleGameQuit = (
    data: { gameId: string },
    socket: socketio.Socket
  ) => {
    logger.info("Trying to quit a game.");
    const game = this.games.get("" + data.gameId);

    if (!game) {
      logger.error(`handleGameQuit(): game "${data.gameId}" was not found.`);
      return;
    }

    const leaver = this.getUserBySocket(socket);
    if (!leaver) {
      logger.error(
        `user whose socketID is "${
          socket.id
        }" could not leave game "${game.getId()}"`
      );
      return;
    }

    if (game.getType() === GameType.analysis) {
      const gameIds = this.gamesPerOwner.get(leaver.getNickname()) || [];
      const gamesIdsToKeep: string[] = [];
      gameIds.forEach((gameId) => {
        if ("" + gameId !== "" + data.gameId) {
          gamesIdsToKeep.push("" + gameId);
        }
      });
      this.gamesPerOwner.set("" + leaver.getNickname(), [...gamesIdsToKeep]);
      this.games.delete("" + data.gameId);

      this.sendDataToClient(
        "socket",
        RealtimeServer.EVENTS.server.game.game_quited,
        { id: data.gameId },
        socket
      );
      this.sendDataToClient(
        "io",
        RealtimeServer.EVENTS.server.match.match_quited,
        { id: data.gameId }
      );
    } else {
      // check what is our role on this game...
      const owner = game.getOwner();
      const opponent = game.getOpponent();

      if (!game.isStarted()) {
        // (we are the owner)
        // remove the game from our memory
        const gamesIdsOwnerIsOwning: string[] = [];
        this.gamesPerOwner
          .get(game.getOwner().getNickname())
          ?.forEach((gId) => {
            if ("" + gId !== "" + game.getId()) {
              gamesIdsOwnerIsOwning.push("" + gId);
            }
          });
        this.gamesPerOwner.set(
          game.getOwner().getNickname(),
          gamesIdsOwnerIsOwning
        );

        if (!this.games.delete("" + game.getId())) {
          console.log("ben alors :')");
        }

        // send game data to owner
        this.sendDataToClient(
          "socket",
          RealtimeServer.EVENTS.server.game.game_quited,
          { id: data.gameId },
          socket
        );

        // send match data to all
        this.sendDataToClient(
          "io",
          RealtimeServer.EVENTS.server.match.match_quited,
          { id: data.gameId }
        );

        logger.info(
          `pending game "${game.getId()}" was removed from owner whose nickname is "${owner.getNickname()}".`
        );

        return;
      }

      if (game.isFinished()) {
        // we were watching our own finished game
        game.removeWatcher(leaver);

        this.sendDataToClient(
          "socket",
          RealtimeServer.EVENTS.server.game.game_quited,
          { id: data.gameId },
          socket
        );

        return;
      }

      if (owner.getNickname() === leaver.getNickname()) {
        // if the other player is still connected, quitting like this is like resigning.
        const opponentInUsers = this.users.findIndex(
          (u) => u.getNickname() === opponent?.getNickname()
        );
        if (opponentInUsers !== -1 && opponent) {
          // update the game so owner is victorious
          game.finish(opponent); // @TODO: be careful about the fact this "opponent" variable was maybe not retrieved from the most reliable source.
          this.games.set("" + game.getId(), game);

          this.updateGamesPerPlayerAndOwnerAfterGameFinishes(
            game,
            owner,
            opponent
          );

          game.getPlayers().forEach((player) => {
            if (player) {
              player.setExperience(player.getExperience() + 1);
              this.updateUserInDB(player);
            }
          });

          if (game.getHistoScores().length > 4) {
            this.saveGameInDB(game);
          }

          // send game data to owner
          this.sendDataToClient(
            "socket",
            RealtimeServer.EVENTS.server.game.game_quited,
            { id: data.gameId },
            socket
          );

          // send game data to opponent
          this.io
            .to(opponent.getSocketId())
            .emit(RealtimeServer.EVENTS.server.game.game_won, {
              content: {
                game: game.toJSON(),
              },
            });

          // send game data to watchers
          game.getWatchers().forEach((watcher) => {
            if (watcher) {
              this.io
                .to(watcher.socketId)
                .emit(RealtimeServer.EVENTS.server.game.game_won, {
                  content: {
                    game: game.toJSON(),
                  },
                });
            }
          });

          // send match data to all
          this.sendDataToClient(
            "io",
            RealtimeServer.EVENTS.server.match.match_won_match,
            {
              match: game.matchToJSON(),
            }
          );
          this.sendDataToClient(
            "io",
            RealtimeServer.EVENTS.server.match.match_won_users,
            {
              users: this.generateUsersJSON(),
            }
          );

          return;
        }

        // if the other player is not connected anymore...

        // ... and game is already finished, this could mean the opponent quitted.
        if (game.isFinished()) {
          console.log("TODO !!");
          return;
        }

        // else, quitting like this is like choosing to "cancel" the game.
        this.games.delete("" + game.getId());
        this.updateGamesPerPlayerAndOwnerAfterGameFinishes(
          game,
          owner,
          opponent
        );

        // send game data to owner
        this.sendDataToClient(
          "socket",
          RealtimeServer.EVENTS.server.game.game_quited,
          { id: data.gameId },
          socket
        );

        // send game data to watchers
        game.getWatchers().forEach((watcher) => {
          if (watcher) {
            this.io
              .to(watcher.socketId)
              .emit(RealtimeServer.EVENTS.server.game.game_quited, {
                content: {
                  id: data.gameId,
                },
              });
          }
        });

        this.sendDataToClient(
          "io",
          RealtimeServer.EVENTS.server.match.match_quited,
          { id: data.gameId }
        );
        return;
      }

      if (opponent?.getNickname() === leaver.getNickname()) {
        // @HERE
        logger.info(
          `Challenger "${leaver.getNickname()}" is about to lose by forfeit, quitting.`
        );

        // if the owner is still connected, quitting like this is like resigning.
        const ownerInUsers = this.users.findIndex(
          (u) => u.getNickname() === owner.getNickname()
        );
        if (ownerInUsers !== -1 && owner) {
          // update the game so the "owner" is victorious
          game.finish(owner); // @TODO: be careful about the fact this variable was maybe not retrieved from the most reliable source.
          this.games.set("" + game.getId(), game);

          // @TODO: update the gamesPerPlayer only: why ??
          this.updateGamesPerPlayerAndOwnerAfterGameFinishes(
            game,
            owner,
            opponent
          );

          game.getPlayers().forEach((player) => {
            if (player) {
              player.setExperience(player.getExperience() + 1);
              this.updateUserInDB(player);
            }
          });

          if (game.getHistoScores().length > 4) {
            this.saveGameInDB(game);
          } else {
            logger.warn(
              `too few moves to save game where game owner "${owner.getNickname()}" was victorious.`
            );
          }

          // send game data to owner
          this.sendDataToClient(
            "socket",
            RealtimeServer.EVENTS.server.game.game_quited,
            { id: data.gameId },
            socket
          );

          // send game data to owner
          this.io
            .to(owner.getSocketId())
            .emit(RealtimeServer.EVENTS.server.game.game_won, {
              content: {
                game: game.toJSON(),
              },
            });

          // send game data to watchers
          game.getWatchers().forEach((watcher) => {
            if (watcher) {
              this.io
                .to(watcher.socketId)
                .emit(RealtimeServer.EVENTS.server.game.game_won, {
                  content: {
                    game: game.toJSON(),
                  },
                });
            }
          });

          // send match data to all
          this.sendDataToClient(
            "io",
            RealtimeServer.EVENTS.server.match.match_won_users,
            {
              users: this.generateUsersJSON(),
            }
          );

          this.sendDataToClient(
            "io",
            RealtimeServer.EVENTS.server.match.match_won_match,
            {
              match: game.matchToJSON(),
            }
          );

          return;
        } else {
          // else, it's like we decide to "cancel" the game
          this.games.delete("" + game.getId());
          this.updateGamesPerPlayerAndOwnerAfterGameFinishes(
            game,
            owner,
            opponent
          );

          // send game data to opponent
          this.sendDataToClient(
            "socket",
            RealtimeServer.EVENTS.server.game.game_quited,
            { id: data.gameId },
            socket
          );

          // send game data to watchers
          game.getWatchers().forEach((watcher) => {
            if (watcher) {
              this.io
                .to(watcher.socketId)
                .emit(RealtimeServer.EVENTS.server.game.game_quited, {
                  content: {
                    id: data.gameId,
                  },
                });
            }
          });

          this.sendDataToClient(
            "io",
            RealtimeServer.EVENTS.server.match.match_quited,
            { id: data.gameId }
          );
          return;
        }
      }

      // so we are probably one of the watchers.
      const watchers = game.getWatchers();
      const myIndexInWatchers = watchers.findIndex(
        (user) => user?.getNickname() === leaver.getNickname()
      );
      if (myIndexInWatchers === -1) {
        logger.error(
          `user "${leaver.getNickname()}" was not found in watchers of game "${game.getId()}".`
        );
        return;
      }
      game.removeWatcher(leaver);
      logger.info(
        `removed watcher "${leaver.getNickname()}" from game "${game.getId()}".`
      );

      this.sendDataToClient(
        "socket",
        RealtimeServer.EVENTS.server.game.game_quited,
        { id: data.gameId },
        socket
      );
      this.sendDataToClient(
        "socket",
        RealtimeServer.EVENTS.server.match.match_unwatch,
        { matches: this.generateMatchesFromGames() },
        socket
      );

      // socket.emit(RealtimeServer.EVENTS.server.game.game_quited, {
      //   content: {
      //     gameId: game.getId(),
      //     matches: this.generateMatchesFromGames()
      //   }
      // });

      // if we are a player and
      // const
      // if ()

      // const gameOwner = game.getOwner();
      // const opponent = game.getOpponent();
      // if (!opponent) {
      //   logger.error(`handleWinByTime(): opponent of game "${data.gameId}" was not found.`);
      //   return;
      // }
      // const gamesOwned: string[] = [];
      // this.gamesPerOwner.get(gameOwner.getNickname())?.forEach(gameId => {
      //   if (gameId !== data.gameId) {
      //     gamesOwned.push(gameId);
      //   }
      // });
      // this.gamesPerOwner.set(gameOwner.getNickname(), gamesOwned);
      // const gamesPlayedForTheOwner: string[] = [];
      // this.gamesPerPlayer.get(gameOwner.getNickname())?.forEach(gameId => {
      //   if (gameId !== data.gameId) {
      //     gamesPlayedForTheOwner.push(gameId);
      //   }
      // });
      // this.gamesPerPlayer.set(gameOwner.getNickname(), gamesPlayedForTheOwner);
      // const gamesPlayedForTheOpponent: string[] = [];
      // this.gamesPerPlayer.get(opponent.getNickname())?.forEach(gameId => {
      //   if (gameId !== data.gameId) {
      //     gamesPlayedForTheOpponent.push(gameId);
      //   }
      // });
      // this.gamesPerPlayer.set(opponent.getNickname(), gamesPlayedForTheOpponent);
      // logger.info(`handleWinByTime(): game "${data.gameId}" was won by time.`);
    }
  };

  private generateUsersJSON = (): JSONUser[] => {
    return this.users.map((e) => e.toJSON());
  };

  private generateMatchesFromGames = (): JSONMatch[] => {
    const matches: JSONMatch[] = [];
    this.games.forEach((game, gameId) => {
      matches.push(game.matchToJSON());
    });
    return matches.reverse();
  };

  private removeAnalysisGamesFromUser = (socket: socketio.Socket) => {
    const gamesIdsToRemove: Game[] = [];

    this.games.forEach((game) => {
      if (game.getType() === GameType.analysis) {
        if (game.getOwner().getSocketId() === socket.id) {
          gamesIdsToRemove.push(game);
        }
      }
    });

    gamesIdsToRemove.map((game) => {
      this.games.delete("" + game.getId());
    });
  };

  /*
  private removeGamesFromUser = (socket: socketio.Socket) => {
    // remove games were he was the only player remaining.

    const gamesIds = this.gamesPerPlayer.get(socket.id);
    gamesIds?.forEach((gameId) => {
      if (this.games.delete(gameId)) {
        logger.log({
          level: "warn",
          message: `Game "${gameId}" deletion (disconnection).`,
        });
      } else {
        logger.log({
          level: "error",
          message: `Game "${gameId}" deletion (disconnection) was not successful.`,
        });
      }
    });

    this.gamesPerPlayer.delete(socket.id);

    // this.matches = this.matches.filter((game) => {
    //   // @TODO: change the code so we remove the game only if it was the remaining player who left the game.
    //   if (game.owner.socketId === socket.id) {
    //     return false;
    //   }
    //   return true;
    // });
  };
  */

  private removeUser = (socket: socketio.Socket) => {
    this.users = this.users.filter((user) => {
      if (user.socketId === socket.id) {
        user.log("Disconnection", "warn");
        return false;
      }
      return true;
    });
    this.sockets = this.sockets.filter((element) => element !== socket);
  };

  private getUserBySocket = (socket: socketio.Socket): User | null => {
    const user = this.users.find((user) => user.socketId === socket.id);
    if (user) {
      return user;
    }

    logger.error(`user with socket id "${socket.id}" not found.`);
    return null;
  };

  private getUserByNickname = (nickname: string): User | null => {
    const user = this.users.find((user) => user.getNickname() === nickname);
    if (user) {
      return user;
    }

    logger.error(`user with nickname "${nickname}" not found.`);
    return null;
  };

  private addWatcherToGame = (
    gameId: string,
    newWatcherr: User
  ): Game | null => {
    const game = this.games.get("" + gameId);

    if (game) {
      game.addWatcher(newWatcherr);
      this.games.set("" + gameId, game);
    } else {
      logger.error("could not find the game with ID='" + gameId + "'");
      return null;
    }
    return game;
  };

  private authenticate = async (socket: socketio.Socket, data: JSONAuth) => {
    console.log(`trying to authenticate user "${data.nickname}"`);

    let newUser: User = this.getUserBySocket(socket) || new User(socket);
    if (!newUser) {
      logger.error(
        `new authentificated user whose socketId is "${socket.id}" was not found.`
      );
      return;
    }

    let language = "en";
    let device = data.device ?? "laptop";
    language = data.language ?? language;
    if (data.nickname !== "") {
      const query = UserDB.where("nickname").equals(data.nickname);

      const userFromDB = await query.findOne().exec();

      if (!userFromDB) {
        logger.error(
          `authenticate(): ${data.nickname} was not found in DATABASE`
        );
        return;
      }

      const userIndex = this.users.findIndex(
        (u) => u.socketId === newUser.socketId
      );
      if (userIndex === -1) {
        logger.error(
          `authenticate(): socket of user "${newUser.nickname}" not found in the list of users maintained server-side.`
        );
        return;
      } else {
        this.users[userIndex].setAuthenticated(true);
        this.users[userIndex].setExperience(userFromDB.experience);
        this.users[userIndex].setNickname(data.nickname);
        this.users[userIndex].setACL(userFromDB.acl);
        this.users[userIndex].setDevice(device);
        this.users[userIndex].setLanguage(language);

        newUser.setAuthenticated(true);
        newUser.setExperience(userFromDB.experience);
        logger.http(`User "${userFromDB.nickname}" logged in.`);

        newUser.setNickname(data.nickname);
        newUser.setACL(userFromDB.acl);
        newUser.setDevice(device);
        newUser.setLanguage(language);
        newUser.logData();

        this.sendDataToClient(
          "socket",
          RealtimeServer.EVENTS.server.user.user_logged_in_users,
          {
            users: this.generateUsersJSON(),
          },
          socket
        );


        const numberOfOwning =
        this.gamesPerOwner.get(data.nickname)?.length || 0;
      const numberOfPlaying =
        this.gamesPerPlayer.get(data.nickname)?.length || 0;
      if (numberOfOwning) {
        logger.info(
          `user whose nickname is "${data.nickname}" was owning "${numberOfOwning}" games.`
        );
        this.gamesPerOwner.get("" + data.nickname)?.forEach((gameId) => {
          const game = this.games.get("" + gameId);
          game?.setOwner(newUser);
          const players = game?.getPlayers();
          if (players) {
            if (players[0]?.getNickname() === data.nickname) {
              game?.setPlayer(newUser, 0);
            }
            if (players[1]?.getNickname() === data.nickname) {
              game?.setPlayer(newUser, 1);
            }
          }
        });
      }

      if (numberOfPlaying) {
        logger.info(
          `user whose nickname is "${data.nickname}" was playing "${numberOfPlaying}" games.`
        );
        this.gamesPerPlayer.get("" + data.nickname)?.forEach((gameId) => {
          const game = this.games.get("" + gameId);
          const players = game?.getPlayers();
          if (players) {
            if (players[0]?.getNickname() === data.nickname) {
              game?.setPlayer(newUser, 0);
            }
            if (players[1]?.getNickname() === data.nickname) {
              game?.setPlayer(newUser, 1);
            }
          }
        });
      }

      if (numberOfPlaying) {
        newUser.setPlaying(true);
      }

      // update onlinePlayerList for all
      this.sendDataToClient(
        "io",
        RealtimeServer.EVENTS.server.user.user_logged_in_users,
        {
          users: this.generateUsersJSON(),
        }
      );
      }
    }
  };

  private sendDataToClient = (
    mode: string,
    eventName: string,
    data: any,
    socket?: socketio.Socket
  ): void => {
    logger.log({
      level: "debug",
      message: `mode: ${mode}, event: ${eventName}`,
    });
    if (mode === "io") {
      this.io.emit(eventName, {
        content: data,
      });
    } else if (mode === "socket") {
      socket &&
        socket.emit(eventName, {
          content: data,
        });
    }
  };

  private updateGamesPerPlayerAndOwnerAfterGameFinishes = (
    game: Game,
    owner: User,
    opponent: User | null
  ) => {
    const gameId = game.getId();
    const gameIds = this.gamesPerOwner.get(owner.getNickname()) || [];
    const gamesIdsToKeep: string[] = [];
    gameIds.forEach((gId) => {
      if ("" + gameId !== gId) {
        gamesIdsToKeep.push(gId);
      }
    });
    this.gamesPerOwner.set("" + owner.getNickname(), [...gamesIdsToKeep]);

    const gamesIdsOwnerIsPlaying: string[] = [];
    this.gamesPerPlayer.get(owner.getNickname())?.forEach((gId) => {
      if (gId !== "" + gameId) {
        gamesIdsOwnerIsPlaying.push(gId);
      }
    });
    if (gamesIdsOwnerIsPlaying.length === 0) {
      owner.setPlaying(false);
    }
    this.gamesPerPlayer.set(
      game.getOwner().getNickname(),
      gamesIdsOwnerIsPlaying
    );

    if (!opponent) {
      logger.error(
        `Could not remove opponent of game owned by "${owner.getNickname()}"`
      );
      return;
    }
    const gamesIdsOpponentIsPlaying: string[] = [];
    this.gamesPerPlayer.get(opponent.getNickname())?.forEach((gId) => {
      if (gId !== "" + gameId) {
        gamesIdsOpponentIsPlaying.push(gId);
      }
    });
    if (gamesIdsOpponentIsPlaying.length === 0) {
      opponent.setPlaying(false);
    }
    this.gamesPerPlayer.set(opponent.getNickname(), gamesIdsOpponentIsPlaying);
  };

  /** DATABASE methods */

  private updateUserInDB = (player: User) => {
    // increment XP
    UserDB.findOne(
      { nickname: player.getNickname() },
      async (err: any, existingUser: any) => {
        if (err) {
          logger.error(`updateUserInDB(): ${err}`);
        }
        if (existingUser) {
          ++existingUser.experience;
          existingUser.save();
          logger.info(`user "${player.getNickname()}" gained 1 XP.`);
        }
      }
    );
  };

  private saveGameInDB = (game: Game) => {
    console.log("trying to save game in DB...");
    const histoVectorsArrowsPositions = game
      .getHistoVectors()
      .map((e) => e.arrowPositions);
    const histoVectorsDirections = game
      .getHistoVectors()
      .map((e) => e.direction);

    const matchDB = new MatchDB({
      date: new Date(),
      players: {
        nicknames: [
          game.getPlayers()[0]?.getNickname(),
          game.getPlayers()[1]?.getNickname(),
        ],
      },
      winner: game.getWinner()?.getNickname(),
      histoScores: game.getHistoScores(),
      histoPositions: game.getHistoPositions(),
      histoTimes: game.getHistoTimes(),
      histoClocks: game.getHistoClocks(),
      histoVectorsArrowsPositions: histoVectorsArrowsPositions,
      histoVectorsDirections: histoVectorsDirections,
      rules: {
        variant: game.getVariant().name,
        timer: {
          player: game.getReferenceClocks(),
        },
      },
    });
    matchDB.save();
  };
}
