export enum vectorDirection {
  "",
  "TOP_RIGHT",
  "RIGHT",
  "BOTTOM_RIGHT",
  "BOTTOM_LEFT",
  "LEFT",
  "TOP_LEFT",
}

export class Vector {
  static NO_DIRECTION = 0;
  static TOP_RIGHT = 1;
  static RIGHT = 2;
  static BOTTOM_RIGHT = 3;
  static BOTTOM_LEFT = 4;
  static LEFT = 5;
  static TOP_LEFT = 6;

  direction: vectorDirection;
  arrowPositions: number[];

  constructor(direction: vectorDirection) {
    this.direction = direction;
    this.arrowPositions = [];
  }

  public addArrowPosition(position: number) {
    this.arrowPositions.push(position);
  }
}
