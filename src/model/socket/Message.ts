import { JSONMessage } from "./server/JSONMessage";

export class Message {
  static id = 0;
  private id: number;
  private date: Date;
  private nickname: string;
  private value: string;

  constructor(nickname: string, value: string) {
    this.id = Message.id++;
    this.date = new Date();
    this.nickname = nickname;
    this.value = value;
  }

  private getFormattedDate(): string {
    return this.date.toLocaleString();
  }

  public toJSON(): JSONMessage {
    return {
      id: this.id,
      date: this.getFormattedDate(),
      nickname: this.nickname,
      value: this.value
    };
  }
}