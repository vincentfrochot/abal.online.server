import { Vector } from "../Vector";

export interface JSONMove {
  gameId: number;
  winningMove: boolean; // weither or not the game should be considered as finished

  lastMoveDate: Date; // will be used to update the timer of the player who just played.
  boardPosition: number[]; // the new board position
  vector: Vector;
  score: number[];
}