export interface JSONMessage {
  id: number;
  date: string;
  nickname: string;
  value: string;
}