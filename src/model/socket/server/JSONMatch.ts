import { GameType } from "../client/JSONGame";

export interface JSONMatch {
  id: number;
  type: GameType;
  nicknameP1: string | null;
  nicknameP2: string | null;
  winner: string | null;
  scoreP1: number;
  scoreP2: number;
  startClockP1: string;
  startClockP2: string;
  variantName: string;
  started: boolean;
  finished: boolean;
}