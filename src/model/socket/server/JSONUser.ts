export interface JSONUser {
  socketId: string;
  nickname: string;
  device: string;
  authenticated: boolean;
  playing: boolean;
  experience: number;
  ping: number;
}