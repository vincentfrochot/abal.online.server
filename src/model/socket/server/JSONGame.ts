import { Vector } from "../Vector";
import { Variant } from "../../Variant";
import { GameType } from "../client/JSONGame";

export interface JSONGame {
  id: number; // mapping
  type: GameType;
  variant: Variant; // name and marbles corresponding to the starting position.
  startClockP1: string; // timer of Player 1 before game starts.
  startClockP2: string;
  owner: string; // nickname

  // social
  challengers: string[] | null; // nicknames
  players: (string | null)[];
  watchers: string[]; // nicknames
  winner: string | null;

  // actual game being played
  started: boolean;
  finished: boolean;
  lastMoveIndex: number; // index of histo variables below
  histoTimes: Date[];
  // e.g. in Belgian Daisy, after a1d4, the array will have this value :
  /*
    [0] => new Date(), <= date of when the start_game event was received
    [1] => new Date() <= date of when the 1st move was received
  */
  histoScores: number[][]; // history of scores for each move
  // e.g. in Belgian Daisy, after a1d4, the array will have this value :
  /*
  [0] => [0,0]
  [1] => [0,0]
  */
  histoVectors: Vector[]; // in order to display arrows on each move played (vectors value are in [0..6]), 0 being no vector.
  // e.g. in Belgian Daisy, after a1d4, the array will have this value :
  /*
  list of marbles which will display an arrow, and direction of the arrow.
  [0] => {
    direction: 0,
    arrowPositions: []
  },
  [1] => {
    direction: 1,
    arrowPositions: [
      42
    ]
  }
  */
  histoPositions: number[][]; // describing marbles position (black are even, white are odd) for each move, including start position.
  // e.g. in Belgian Daisy, after a1d4, the array will have this value :
  /*
    [0] => [
    0,
    2,
    10,
    12,
    14,
    24,
    26,
    94,
    96,
    106,
    108,
    110,
    118,
    120, // up to this point, black marbles, since those are even numbers
    7,
    9,
    17,
    19,
    21,
    31,
    33,
    89,
    91,
    101,
    103,
    105,
    113,
    115
  ],
    [1] => [
    2,
    10,
    12,
    14,
    24,
    26,
    94,
    96,
    106,
    108,
    110,
    118,
    120,
    7,
    9,
    17,
    19,
    21,
    31,
    33,
    89,
    91,
    101,
    103,
    105,
    113,
    115,
    42 // the black marble that moved from 0 to 42.
    ]
  */
}
