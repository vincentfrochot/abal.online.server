import { User } from "./User";
import logger from "../../util/logger";
import { timeStamp } from "console";
import { JSONMatch } from "./server/JSONMatch";
import { JSONGame } from "./server/JSONGame";
import { JSONGame as JSONGameClient, GameType } from "./client/JSONGame";
import { JSONMove } from "./server/JSONMove";
import { Variant } from "../Variant";
import { Vector } from "./Vector";

// this class represent a real game between two players. We want to display the board position.
// @TODO: add the reason of the end (F=>forfeit, R=>resign, T=>time, S=>score, D=>draw)
export class Game {
  static id = 0;
  private id: number;
  private ownerIsP2: boolean; // cast to int to get the indice of the players array
  private owner: User; // used to display "game of <xxx>"
  private type: GameType;
  private variant: Variant;
  private startClocks: string[]; // value is decremented each time
  private referenceClocks: string[]; // starting values

  // social
  private challengers: User[];
  private players: (User | null)[];
  private watchers: User[];
  private winner: User | null; // /!\ there may be no winner : draw || double forfeit.

  // real game
  private started: boolean;
  private finished: boolean;
  private lastMoveIndex: number;
  // private histoMoves: string[][];
  private histoScores: number[][];
  private histoVectors: Vector[];
  private histoPositions: number[][];
  private histoTimes: Date[]; // contains dates of when moves were received by the server. First date is when game started though.
  private histoClocks: number[]; // contains updated remaining time in seconds after each move

  //  roomId: string; // used for communications ?

  constructor(owner: User, data: JSONGameClient) {
    this.id = Game.id++;
    this.owner = owner;
    this.type = data.type;
    this.variant = new Variant(data.variant.name, data.variant.boardPosition);
    // console.log(data.variant.boardPosition);
    this.startClocks = [data.clockP1, data.clockP2];
    this.referenceClocks = [data.clockP1, data.clockP2];
    if (this.type === GameType.analysis) {
      this.startClocks = ["0", "0"];
    }

    this.challengers = [];
    this.players = [];
    this.players[0] = null;
    this.players[1] = null;
    if (data.player1 === true) {
      this.ownerIsP2 = false;
      this.players[0] = owner;
    } else {
      this.ownerIsP2 = true;
      this.players[1] = owner;
    }
    this.watchers = [];
    this.winner = null;

    this.started = false;
    if (this.type === GameType.analysis) {
      this.started = true;
    }
    this.finished = false;
    this.lastMoveIndex = 0;
    this.histoScores = [];
    this.histoScores.push([data.scoreP1, data.scoreP2]);
    // this.histoMoves = [];
    this.histoVectors = [];
    this.histoVectors.push(new Vector(Vector.NO_DIRECTION));
    this.histoPositions = [];
    this.histoPositions.push(data.variant.boardPosition);
    this.histoTimes = [];
    if (this.type === GameType.analysis) {
      this.histoTimes.push(new Date());
    }
    this.histoClocks = [];
  }

  public static createAnalysis = (owner: User, referenceGame: Game): Game => {
    const type = GameType.analysis;
    const variant = referenceGame.variant;
    const clockP1 = "0";
    const clockP2 = "0";
    const player1 = true;
    const analysisGame = new Game(owner, {
      id: 0,
      type,
      variant,
      clockP1,
      clockP2,
      player1,
      scoreP1: referenceGame.histoScores[referenceGame.lastMoveIndex][0],
      scoreP2: referenceGame.histoScores[referenceGame.lastMoveIndex][1],
    });
    analysisGame.lastMoveIndex = referenceGame.lastMoveIndex;
    analysisGame.histoPositions = [...referenceGame.histoPositions];
    analysisGame.histoScores = [...referenceGame.histoScores];
    analysisGame.histoTimes = [...referenceGame.histoTimes];
    analysisGame.histoVectors = [...referenceGame.histoVectors];

    return analysisGame;
  };

  public matchToJSON(): JSONMatch {
    return {
      id: this.id,
      type: this.type,
      variantName: this.variant.name,
      nicknameP1: this.players[0] ? this.players[0].nickname : null,
      nicknameP2: this.players[1] ? this.players[1].nickname : null,
      winner: this.winner ? this.winner.nickname : null,
      scoreP1: this.histoScores[this.lastMoveIndex][0],
      scoreP2: this.histoScores[this.lastMoveIndex][1],
      startClockP1: this.startClocks[0],
      startClockP2: this.startClocks[1],
      started: this.started,
      finished: this.finished,
    };
  }

  public toJSON(): JSONGame {
    return {
      id: this.id,
      type: this.type,
      variant: this.variant,
      startClockP1: this.startClocks[0],
      startClockP2: this.startClocks[1],
      owner: this.owner.nickname,
      winner: this.winner ? this.winner.nickname : null,

      challengers: this.challengers.map((e) => e?.nickname),
      players: this.players.map((e) => {
        return e ? e.nickname : null;
      }),
      watchers: this.watchers.map((e) => e?.nickname),

      started: this.started,
      finished: this.finished,
      lastMoveIndex: this.lastMoveIndex,
      histoScores: this.histoScores,
      histoVectors: this.histoVectors,
      histoPositions: this.histoPositions,
      histoTimes: this.histoTimes,
    };
  }
  /*
  public moveToJSON(): JSONMove {
    return {
      lastMoveDate: this.his
    };
  }
*/

  public playMove = (data: {
    gameId: number;
    computedMove: {
      move: { newCoordinates1D: number[]; oldCoordinates1D: number[] };
      vector: { arrowPositions: number[]; direction: number };
    };
    pushed: boolean;
  }) => {
    const marbles = this.histoPositions[this.lastMoveIndex].filter(
      (element) => !data.computedMove.move.oldCoordinates1D.includes(element)
    );
    data.computedMove.move.newCoordinates1D.forEach((element) => {
      marbles.push(element);
    });

    const vector = new Vector(data.computedMove.vector.direction);
    data.computedMove.vector.arrowPositions.forEach((e) => {
      vector.addArrowPosition(e);
    });
    this.histoVectors.push(vector);
    this.histoPositions.push(marbles);
    if (data.pushed) {
      let scoreP1 = this.histoScores[this.lastMoveIndex][0];
      let scoreP2 = this.histoScores[this.lastMoveIndex][1];
      if (this.lastMoveIndex % 2 === 0) {
        if (++scoreP1 > 5) {
          this.finished = true;
          this.winner = this.players[0];
        }
      } else {
        if (++scoreP2 > 5) {
          this.finished = true;
          this.winner = this.players[1];
        }
      }
      this.histoScores.push([scoreP1, scoreP2]); // @TODO: fix this !! fix what ?
    } else {
      this.histoScores.push(this.histoScores[this.lastMoveIndex]);
    }
    const date = new Date();
    const timeDiffInSec = Math.floor(
      (date.getTime() - this.histoTimes[this.lastMoveIndex].getTime()) / 1000
    );
    const clockTimeAsString = this.startClocks[this.lastMoveIndex % 2].split(
      "."
    );
    const clockTimeInSecForMinutes = parseInt(clockTimeAsString[0]) * 60;
    let clockTimeInSecForSeconds = 0;
    if (clockTimeAsString[1]) {
      clockTimeInSecForSeconds = parseInt(clockTimeAsString[1]);
    }
    const clockTimeInSeconds =
      clockTimeInSecForMinutes + clockTimeInSecForSeconds;
    const newClockTimeInSeconds = clockTimeInSeconds - timeDiffInSec;
    if (newClockTimeInSeconds < 0) {
      this.finished = true;
      this.winner = this.players[(this.lastMoveIndex + 1) % 2];
    }
    const newClockTimeMinutes = Math.floor(newClockTimeInSeconds / 60);
    const secRatio = newClockTimeInSeconds / 60 - newClockTimeMinutes;
    let newClockTimeSec = "" + Math.floor(secRatio * 60);
    if (parseInt(newClockTimeSec) < 10) {
      newClockTimeSec = "0" + newClockTimeSec;
    }
    this.startClocks[this.lastMoveIndex % 2] =
      "" + newClockTimeMinutes + "." + newClockTimeSec;
    this.histoTimes.push(date);
    if (!isNaN(newClockTimeInSeconds)) {
      this.histoClocks.push(newClockTimeInSeconds);
    }
    this.lastMoveIndex++;
  };

  public playAnalysisMove = (data: {
    gameId: number;
    computedMove: {
      move: { newCoordinates1D: number[]; oldCoordinates1D: number[] };
      vector: { arrowPositions: number[]; direction: number };
    };
    pushed: boolean;
    index: number;
  }) => {
    const marbles = this.histoPositions[this.lastMoveIndex].filter(
      (element) => !data.computedMove.move.oldCoordinates1D.includes(element)
    );
    data.computedMove.move.newCoordinates1D.forEach((element) => {
      marbles.push(element);
    });

    const vector = new Vector(data.computedMove.vector.direction);
    data.computedMove.vector.arrowPositions.forEach((e) => {
      vector.addArrowPosition(e);
    });
    this.histoVectors.push(vector);
    this.histoPositions.push(marbles);
    const previousScore = this.histoScores[this.lastMoveIndex];
    if (data.pushed) {
      let scoreP1 = this.histoScores[this.lastMoveIndex][0];
      let scoreP2 = this.histoScores[this.lastMoveIndex][1];
      if (this.lastMoveIndex % 2 === 0) {
        if (++scoreP1 > 5) {
          this.finished = true;
          this.winner = this.players[0];
        }
      } else {
        if (++scoreP2 > 5) {
          this.finished = true;
          this.winner = this.players[1];
        }
      }
      this.histoScores.push([scoreP1, scoreP2]); // @TODO: fix this !!
    } else {
      this.histoScores.push(this.histoScores[this.lastMoveIndex]);
    }
    this.lastMoveIndex++;
  };

  public checkWinByTime = () => {
    const date = new Date();
    const timeDiffInSec = Math.floor(
      (date.getTime() - this.histoTimes[this.lastMoveIndex].getTime()) / 1000
    );
    const clockTimeAsString = this.startClocks[this.lastMoveIndex % 2].split(
      "."
    );
    const clockTimeInSecForMinutes = parseInt(clockTimeAsString[0]) * 60;
    let clockTimeInSecForSeconds = 0;
    if (clockTimeAsString[1]) {
      clockTimeInSecForSeconds = parseInt(clockTimeAsString[1]);
    }
    const clockTimeInSeconds =
      clockTimeInSecForMinutes + clockTimeInSecForSeconds;
    const newClockTimeInSeconds = clockTimeInSeconds - timeDiffInSec;
    if (newClockTimeInSeconds < 0) {
      this.finished = true;
      this.winner = this.players[(this.lastMoveIndex + 1) % 2];
      return true;
    }
    return false;
  };

  public undoLastMove = () => {
    if (this.lastMoveIndex > 0) {
      this.histoPositions.pop();
      this.histoVectors.pop();
      this.histoScores.pop();
      this.histoTimes.pop();
      this.finished = false;
      this.lastMoveIndex--;
    }
  };

  public start(opponent: User) {
    this.players[+!this.ownerIsP2] = opponent;
    this.started = true;
    this.histoTimes.push(new Date());
  }

  public finish = (winner: User | null) => {
    this.finished = true;
    if (winner) {
      this.winner = winner;
    }
  };

  public setOwner(owner: User) {
    this.owner = owner;
  }

  public setPlayer(player: User, index: number) {
    this.players[index] = player;
  }

  public addChallenger(challenger: User) {
    this.challengers = [...this.challengers, challenger];
  }

  public removeChallenger = (challenger: User) => {
    this.challengers = this.challengers.filter((element) => element !== challenger);
  };

  public addWatcher(watcher: User) {
    this.watchers = [...this.watchers, watcher];
  }

  public removeWatcher(watcher: User) {
    this.watchers = this.watchers.filter((element) => element !== watcher);
  }

  public log(action: string, level: string) {
    logger.log({
      level: level,
      message: `Game ${action} from "${this.owner.nickname}".`,
    });
  }

  public getId(): number {
    return this.id;
  }

  public isStarted = (): boolean => {
    return this.started;
  };

  public isFinished = (): boolean => {
    return this.finished;
  };

  public getType = (): GameType => {
    return this.type;
  };

  public getOwner(): User {
    return this.owner;
  }

  public getOpponent = (): User | null => {
    return this.players[+!this.ownerIsP2];
  };

  public getOpponentFromMyNickname = (nickname: string): User | null => {
    if (!this.players[0] || !this.players[1]) {
      return null;
    }

    if (this.players[0].getNickname() === nickname) {
      return this.players[1];
    }

    if (this.players[1].getNickname() === nickname) {
      return this.players[0];
    }

    return null;
  };

  public getPlayers = (): (User | null)[] => {
    return this.players;
  };

  public getWatchers = (): (User | null)[] => {
    return this.watchers;
  };

  public getChallengers = (): (User | null)[] => {
    return this.challengers;
  };

  public getWinner = (): (User | null) => {
    return this.winner;
  };

  public getHistoScores = (): number[][] => {
    return this.histoScores;
  };

  public getHistoPositions = (): number[][] => {
    return this.histoPositions;
  };

  public getHistoVectors = (): Vector[] => {
    return this.histoVectors;
  };

  public getHistoTimes = (): Date[] => {
    return this.histoTimes;
  };

  public getHistoClocks = (): number[] => {
    return this.histoClocks;
  };

  public getVariant = (): Variant => {
    return this.variant;
  };

  public getStartClocks = (): String[] => {
    return this.startClocks;
  };

  public getReferenceClocks = (): String[] => {
    return this.referenceClocks;
  };

  public incrementId() {
    this.id = ++Game.id;
  }

  public setType = (type: GameType) => {
    this.type = type;
  };
}
