export class ACL {
  // default ACL for guests 1/2
  chatRead: boolean = true;
  chatWrite: boolean;
  gameSpectate: boolean = true;
  gameCreate: boolean; // @TODO later : if we want guests to play, we probably need some changes in the code to handle the fact this is not a "regular" user for who we could store games server side using his nickname (risks of losing the game)
  analysisSpectate: boolean = true;
  analysisCreate: boolean;

  constructor(authenticated: boolean = false, admin: boolean = false) {
    // overwriting default ACL in case user would be authenticated (but is it likely he is not...) 2/2

    this.chatWrite = authenticated;
    this.gameCreate = authenticated;
    this.gameCreate = authenticated;
    this.analysisCreate = authenticated;
  }

  // overwriting user ACL depending on database
  public updateACL = (authenticated: boolean = false, admin: boolean = false, userACL: ACL|null) : ACL => {
    this.chatWrite = authenticated;
    this.gameCreate = authenticated;
    this.gameCreate = authenticated;
    this.analysisCreate = authenticated;
    if (userACL) {
      this.chatRead = userACL.chatRead;
      this.chatWrite = userACL.chatWrite;
      this.gameSpectate = userACL.gameSpectate;
      this.gameCreate = userACL.gameCreate;
      this.analysisSpectate = userACL.analysisSpectate;
      this.analysisCreate = userACL.analysisCreate;
    }
    return this;
  };

  public toJSON = () => {
    return {
      chatRead: this.chatRead,
      chatWrite: this.chatWrite,
      gameSpectate: this.gameSpectate,
      gameCreate: this.gameCreate,
      analysisSpectate: this.analysisSpectate,
      analysisCreate: this.analysisCreate,
    };
  }
}
