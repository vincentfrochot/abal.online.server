export interface JSONMove {
  gameId: number;
  from: number; // starting square
  to: number; // target square
  push: boolean; // not necessarily pushed out of the board. just if it is a push or not.
  boardPosition: number[]; // the board position BEFORE the move happened. We want the server to compute the new position.
}