export interface JSONAuth {
  nickname: string;
  device: string;
  language: string | null; // prefered language, if chosen
  experience: number; // number of games played
}