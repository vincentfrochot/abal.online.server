import { Variant } from "../../Variant";

export enum GameType {
  "match" = "match",
  "analysis" = "analysis"
}

/*
  JSONGame is received after the game configuration has been submitted.
*/
export interface JSONGame {
  id: number;
  type: GameType;
  variant: Variant;
  player1: boolean;
  scoreP1: number;
  scoreP2: number;
  clockP1: string; // e.g. 15.30 => 15 minutes and 30 seconds
  clockP2: string;
}