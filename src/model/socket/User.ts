import logger from "../../util/logger";
import { JSONUser } from "./server/JSONUser";
import { ACL } from "./ACL";

export class User {
  // socket: SocketIO.Socket; // @TODO: check if it will be used for communications
  // apparently when this "socket" property is there, the app crashes. "maximal stack size exceeded"
  socketId: string;
  canUseIp: boolean;
  ip: string;
  connectTime: Date;
  device: string;
  nickname: string;
  language: string;
  authenticated: boolean;
  playing: boolean;
  experience: number;
  ping: number;
  acl: ACL;

  constructor(socket: SocketIO.Socket) {
    // this.socket = socket;
    this.socketId = socket.id;
    this.ip = socket.request.headers["x-real-ip"];
    this.canUseIp = this.ip !== "" && this.ip !== undefined;
    this.connectTime = new Date();
    this.device = "";
    this.nickname = "Guest-" + this.socketId.substr(-4);
    this.language = "";
    this.authenticated = false;
    this.acl = new ACL();
    this.playing = false;
    this.experience = 0;
    this.ping = 0;
    // @TODO: could be nice to make a request to MongoDB here, to retrieve some data about this user.
    // e.g. XP (number of games played). But these data are already retrieved from the auth call API using the login button. So ... ?
    // Anyway, still need to be able to write in database from realtime server, so it's the realtime server that decides when he saves a game for example.
  }

  public toJSON(): JSONUser {
    return {
      socketId: this.socketId,
      nickname: this.nickname,
      device: this.device,
      authenticated: this.authenticated,
      playing: this.playing,
      experience: this.experience,
      ping: this.ping,
    };
  }

  public logout() {
    this.nickname = "Guest-" + this.socketId.substr(-4);
    this.experience = 0;
    this.playing = false;
    this.authenticated = false;
  }

  public getSocketId = (): string => {
    return this.socketId;
  }

  public getNickname = (): string => {
    return this.nickname;
  }

  public isAuthenticated = (): boolean => {
    return this.authenticated;
  }

  public getExperience = (): number => {
    return this.experience;
  };

  public getPing = (): number => {
    return this.ping;
  };

  public getACL = (): ACL => {
    return this.acl;
  };

  public setACL = (acl: ACL): void => {
    this.acl = acl;
  };

  // public getACLJSON = (): mixed => {
  //   return this.acl.toJSON();
  // };

  public getIP = (): string => {
    return this.ip;
  };

  public updateACL = (userACL: ACL|null) => {
    this.acl = this.acl.updateACL(this.authenticated, this.nickname === "vincent", userACL);
  }

  public setPlaying = (isPlaying: boolean) => {
    this.playing = isPlaying;
  }

  public setAuthenticated(auth: boolean) {
    this.authenticated = auth;
  }

  public setDevice(device: string) {
    this.device = device;
  }

  public setNickname(nickname: string) {
    this.nickname = nickname;
  }

  public setLanguage(language: string) {
    this.language = language;
  }

  public setExperience(xp: number) {
    this.experience = xp;
  }

  public setPing = (ping: number) => {
    this.ping = ping;
  };

  public log(action: string, level: string) {
    const ip = this.canUseIp ? this.ip : "anonymous";
    logger.log({
      level: level,
      message: `${action} from "${this.socketId}" (${ip}).`,
    });
  }

  public logData() {
    logger.log({
      level: "info",
      message: `${this.socketId} nickname: "${this.nickname}", device: "${this.device}", language: "${this.language}".`,
    });
  }
}
