import { User } from "./User";
import logger from "../../util/logger";

// This class represent a list element. We just want to display synthetic data about matches being played.

// @deprecated
export class Match {
  static id = 0;
  private id: number;
  private ownerSocketId: string;
  private ownerNickname: string;
  private opponentNickname: string | null;
  private variant: string;
  private scoreP1: number;
  private scoreP2: number;
  private time: string;
  private ownerIsP1: boolean;
  private started: boolean;
  private finished: boolean; // we display finished game while at least 1 player is on it.

  constructor(
    ownerSocketId: string,
    ownerNickname: string,
    variant: string,
    scoreP1: number,
    scoreP2: number,
    time: string,
    ownerIsP1: boolean,
    started: boolean = false,
    finished: boolean = false,
    opponentNickname: string | null = null
  ) {
    this.ownerSocketId = ownerSocketId;
    this.ownerNickname = ownerNickname;
    this.variant = variant;
    this.scoreP1 = scoreP1;
    this.scoreP2 = scoreP2;
    this.time = time;
    this.ownerIsP1 = ownerIsP1;
    this.started = started;
    this.finished = finished;
    this.opponentNickname = opponentNickname;
    this.id = Match.id++;
  }

  public log(action: string) {
    const date = new Date();
    logger.log({
      level: "info",
      message: `[${date.toLocaleString()}] ${action} from ${
        this.ownerNickname
      }.`,
    });
  }

  public setOpponentNickname(nickname: string) {
    this.opponentNickname = nickname;
  }

  public getId(): number {
    return this.id;
  }

  public getOwnerSocketId(): string {
    return this.ownerSocketId;
  }

  public getOwnerNickname(): string {
    return this.ownerNickname;
  }

  public getStarted(): boolean {
    return this.started;
  }

  public getFinished(): boolean {
    return this.finished;
  }

  public getOpponentNickname(): string | null {
    return this.opponentNickname;
  }

  public getVariant(): string {
    return this.variant;
  }

  public getScoreP1(): number {
    return this.scoreP1;
  }

  public getScoreP2(): number {
    return this.scoreP2;
  }

  public getTime(): string {
    return this.time;
  }

  public getOwnerIsP1(): boolean {
    return this.ownerIsP1;
  }
}
