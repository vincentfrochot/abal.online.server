import { Schema, Model, model, Document } from "mongoose";

// Mongoose: a schema defines the shape of the document within that collection
let MatchSchema: Schema = new Schema( // @todo: add SGF field
  {
    date: {
      type: Schema.Types.Date,
    },
    players: {
      nicknames: {
        // nickname is unique in database so we can find these users
        type: [Schema.Types.String],
      },
    },
    winner: {
      // winner nickname
      type: Schema.Types.String,
    },
    histoScores: {
      type: [[Schema.Types.Number]],
    },
    histoPositions: {
      type: [[Schema.Types.Number]],
    },
    histoVectorsArrowsPositions: {
      type: [[Schema.Types.Number]],
    },
    histoVectorsDirections: {
      type: [Schema.Types.String]
    },
    histoTimes: {
      type: [Schema.Types.Date]
    },
    histoClocks: {
      type: [Schema.Types.Number]
    },
    rules: { // start conditions
      variant: {
        type: Schema.Types.String, // name of the variant
      },
      timer: {
        // clocks times are expressed in seconds
        player: {
          // players can have a different time if they have a huge difference in level of play
          type: [Schema.Types.String],
        },
        move: {
          // game is lost if you play in more time than this
          type: [Schema.Types.Number],
        },
        bonus_move: {
          // time increment after a move was played
          type: [Schema.Types.Number],
        },
        bonus_score: {
          // time increment after a marble was pushed out
          type: [Schema.Types.Number],
        },
      },
      tags: {
        type: [Schema.Types.String], // e.g. ["blitz", "daisy", "diagonal"]
      },
    },
    context: {
      title: {
        type: Schema.Types.String,
      },
      description: {
        type: Schema.Types.String,
      },
      video: {
        type: Schema.Types.String,
      },
    },
    tags: {
      type: [Schema.Types.String], // e.g. ["MSO", "David Pearce", "winInK"] if the endgame contains a "win In K" puzzle ? "abal.online" or "migs" if the game was played on abal.online or migs
    },
  },
  {
    timestamps: {},
  }
);
export { MatchSchema };

export interface IMatch { // interface representing an object from database
  _id: any;
  players: {
    nicknames: string[];
  };
  rules: {
    timer: {
      player: string[];
      move: string[];
      bonus_move: string[];
      bonus_score: string[];
    },
    tags: string[],
    variant: string
  };
  histoScores: number[][];
  histoPositions: number[][];
  histoVectorsArrowsPositions: number[][];
  histoVectorsDirections: string[];
  histoTimes: Date[];
  histoClocks: number[];
  tags: string[];
  date: Date;
  winner: string;
  createdAt: Date;
  updatedAt: Date;
}

// Mongoose: Instances of Models are documents. Documents have many of their own built-in instance methods. We may also define our own custom document instance methods too.
export interface IMatchModel extends IMatch, Document {}

// Allow instanciation of User objects.
export const Match: Model<IMatchModel> = model<IMatchModel>(
  "Match",
  MatchSchema
);
