export interface JSONReplayMatchListElement {
  id: string;
  date: string;
  winner: string;
  playersNicknames: string[];
  score: number[];
  gameLength: number;
  variantName: string;
}