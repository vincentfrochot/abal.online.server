export interface JSONUser {
  id: string;
  nickname: string;
  experience: number;
  createdAt: Date;
}