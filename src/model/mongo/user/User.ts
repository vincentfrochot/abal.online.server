import { Schema, Model, model, Document } from "mongoose";
import { ACL } from "../../socket/ACL";

// Mongoose: a schema defines the shape of the document within that collection
let UserSchema: Schema = new Schema(
  {
    nickname: {
      type: Schema.Types.String,
      required: "required.nickname",
      alias: "pseudo",
      unique: true, // imply index: true
      lowercase: true,
      minlength: 3,
      maxlength: 15,
    },
    email: {
      type: Schema.Types.String,
      required: "required.email",
      unique: "email should be unique",
      lowercase: true,
      validate: [
        (email: string): boolean => {
          const emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
          return emailRegex.test(email);
        },
        "not valid",
      ],
    },
    salt: {
      type: Schema.Types.String,
    },
    password: {
      type: Schema.Types.String,
      required: "required.password",
    },
    elo: {
      // elo based on offline tournaments
      type: Schema.Types.Number,
    },
    experience: {
      type: Schema.Types.Number,
      default: 0,
    },
    acl: {
      chatRead: {
        type: Schema.Types.Boolean,
        default: true,
      },
      chatWrite: {
        type: Schema.Types.Boolean,
        default: false, // ACL chat: by default, do not allow write access to chat. Allow manually based on what blocked test messages were.
      },
      gameSpectate: {
        type: Schema.Types.Boolean,
        default: true,
      },
      gameCreate: {
        type: Schema.Types.Boolean,
        default: true,
      },
      analysisSpectate: {
        type: Schema.Types.Boolean,
        default: true,
      },
      analysisCreate: {
        type: Schema.Types.Boolean,
        default: true,
      },
    },
    firstname: {
      type: Schema.Types.String,
    },
    lastname: {
      type: Schema.Types.String,
    },
    lastLogin: {
      type: Schema.Types.Date,
    },
  },
  {
    timestamps: {},
  }
);
UserSchema.statics.findBy = async function (
  field: string,
  value: string
): Promise<any[]> {
  let query = Model.find();
  query.where(field).equals(new RegExp(value, "i"));
  return await query.exec();
};
export { UserSchema };

export interface IUser {
  id?: any;
  nickname: string;
  email: string;
  password: string; // encrypted value
  salt: string;
  experience: number;
  elo: number;
  firstname: string;
  lastname: string;
  // name?: { first: string, last: string };
  acl: ACL;
  lastLogin: Date;
  createdAt: Date;
  updatedAt: Date;
  encryptPassword: (password: string) => string;
  can: (action: ACL) => boolean;
  canSignIn: () => boolean;
  canReadChat: () => boolean;
}

// Allow instanciation of User objects.
export const User: Model<IUserModel> = model<IUserModel>("User", UserSchema);
// e.g. let user = new User();

// Mongoose: Instances of Models are documents. Documents have many of their own built-in instance methods. We may also define our own custom document instance methods too.
export interface IUserModel extends IUser, Document {
  fullName(): string;
}

export class OnlineUser {
  socketId: string;
  user: IUser | null = null;
  authenticated: boolean = false;
  nickname: string | null = null;
  experience: number | null = null;
  country: string | null = null;
  isMobile: boolean = false;
  ip: string | null = null;
  // acl: ACL;

  constructor(socketId: string) {
    this.socketId = socketId;
  }

  getSocketId() {
    return this.socketId;
  }

  getNickname() {
    return this.nickname;
  }
  setNickname(nickname: string) {
    this.nickname = nickname;
  }

  getExperience() {
    return this.experience;
  }
  setExperience(experience: number) {
    this.experience = experience;
  }

  getCountry() {
    return this.country;
  }
  setCountry(country: string) {
    this.country = country;
  }

  getIsMobile() {
    return this.isMobile;
  }
  setIsMobile(isMobile: boolean) {
    this.isMobile = isMobile;
  }

  getIp() {
    return this.ip;
  }
  setIp(ip: string) {
    this.ip = ip;
  }

  isAuthenticated() {
    return this.authenticated;
  }
  authenticate() {
    this.authenticated = true;
  }
  disconnect() {
    this.authenticated = false;
  }
}
