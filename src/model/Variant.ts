export class Variant {
  name: string;
  boardPosition: number[];

  constructor(name: string, boardPosition: number[]) {
    this.name = name;
    this.boardPosition = [...boardPosition];
  }
}