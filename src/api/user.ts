import { Request, Response } from "express";
import logger from "../util/logger";
import { User } from "../model/mongo/user/User";
import { randomBytes } from "crypto";
import * as argon2 from "argon2";
import { JSONUser } from "../model/mongo/user/JSONUser";

const checkEmailExists = (): boolean => {
  return true;
};

const signUp = () => {};

export const getList = async (req: Request, res: Response) => {
  User.find({}, function(err, users) {
    const usersToReturn: JSONUser[] = [];
    users.map((u) => {
      usersToReturn.push(
        {
          id: u._id,
          nickname: u.nickname,
          experience: u.experience,
          createdAt: u.createdAt
        }
      );
    });

    res.status(200).json({
      content: usersToReturn
    });
  });
};

export const logIn = async (req: Request, res: Response) => {
  if (!req.body) {
    return res.status(500).json({
      error: "No data received.",
    });
  }
  if (!req.body.nickname) {
    return res.status(500).json({
      error: "Please provide nickname.",
    });
  }
  if (!req.body.email) {
    return res.status(500).json({
      error: "Please provide email.",
    });
  }
  if (!req.body.password) {
    return res.status(500).json({
      error: "Please provide password.",
    });
  }

  const mail = req.body.email.trim();
  const nickname = req.body.nickname;
  const password = req.body.password;

  if (password.length < 6) {
    return res.status(404).json({
      error: "password length should be higher than 5."
    });
  }

  const salt = randomBytes(32);
  const passwordHashed = await argon2.hash(password, { salt });

  const user = new User({
    nickname: nickname,
    email: mail,
    password: passwordHashed,
    salt: salt.toString("hex")
  });

  User.findOne({ $and: [ {email: req.body.email}, {nickname: req.body.nickname} ] }, async (err, existingUser) => {
    if (err) {
      return res.status(404).json({
        error: "Error while trying to read from DB."
      });
    }
    if (existingUser) { // SIGN IN
      // now we can compare passwords
      const auth = await argon2.verify(existingUser.password, password);

      if (auth) {
        existingUser.lastLogin = new Date();
        existingUser.save();
        logger.http(req.body.email + " sign in SUCCESS");
        const defaultAcl = {
          readChat: true,
          writeChat: true,
          createGame: true,
          rejoinGame: true,
          createAnalysis: true
        };
        const adminAcl = {
          readChat: true,
          writeChat: true,
          createGame: true,
          rejoinGame: true,
          createAnalysis: true
        };

        return res.status(200).json({
          content: {
            nickname,
            acl: existingUser.acl
          }
        });
      }

      return res.status(404).json({
        error: "This Email is already used !"
      });
    }

    // SIGN UP
    user.save(err => {
      if (err) {
        logger.error(err);
        if (err.code === 11000) {
          return res.status(404).json({
            error: "Bad credentials."
          });
        } else {
          logger.error(err.message);
          return res.status(404).json({
            error: err.message
          });
        }
      }
      logger.http(mail + " sign up SUCCESS");

      return res.status(200).json({
        content: {
          nickname,
          acl: {
            readChat: true,
            writeChat: true,
            createGame: true,
            rejoinGame: true,
            createAnalysis: false
          }
        }
      });
    });
  });
};