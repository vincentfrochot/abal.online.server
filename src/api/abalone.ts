import { Match } from "../model/mongo/abalone/Match";
import { Request, Response } from "express";
import { JSONReplayMatchListElement } from "../model/mongo/abalone/JSONReplayMatchListElement";
import logger from "../util/logger";

const escapeIncomingData = (data: any) => {
  return data.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};

export const getList = async (req: Request, res: Response) => {
  const page = req.query.page ? parseInt("" + req.query.page, 10) - 1 : 0;
  if (req.query.nickname) {
    const playerNickname = req.query.nickname || "";
    logger.http(`getting last games of users starting with "${playerNickname}"`);

    const regexp = new RegExp(escapeIncomingData(playerNickname), "gi");

    Match.find({ "players.nicknames": regexp })
      .sort("-_id")
      .skip(page * 100)
      .limit(100)
      .exec(function (err, matchs) {
        if (err) {
          console.log(err);
          return err;
        }
        const matches: JSONReplayMatchListElement[] = [];

        matchs.map((m) => {
          const gameLength = m.histoScores.length - 1;
          matches.push({
            id: m._id,
            date: m.date.toLocaleString(),
            winner: m.winner,
            playersNicknames: m.players.nicknames,
            score: m.histoScores[gameLength],
            gameLength: gameLength,
            variantName: m.rules.variant,
          });
        });

        res.status(200).json({
          content: matches,
        });
      });
  } else {
    Match.find({})
      .sort("-_id")
      .skip(page * 100)
      .limit(100)
      .exec(function (err, matchs) {
        if (err) {
          console.log(err);
          return err;
        }

        const matches: JSONReplayMatchListElement[] = [];

        matchs.map((m) => {
          const gameLength = m.histoScores.length - 1;
          matches.push({
            id: m._id,
            date: m.date.toLocaleString(),
            winner: m.winner,
            playersNicknames: m.players.nicknames,
            score: m.histoScores[gameLength],
            gameLength: gameLength,
            variantName: m.rules.variant,
          });
        });

        res.status(200).json({
          content: matches,
        });
      });
  }
};

export const getMatch = async (req: Request, res: Response) => {
  if (!req.query.gameId) {
    return res.status(500).json({
      error: "Please provide game id.",
    });
  }
  Match.findById(req.query.gameId, function (err, match) {
    res.status(200).json({
      content: match,
    });
  });
};
