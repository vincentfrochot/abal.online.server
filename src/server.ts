import app from "./app";
import logger from "./util/logger";
import { WEBSITE_URL } from "./util/secrets";

/**
 * Start Express server.
 */
const server = app.listen(app.get("port"), () => {
  logger.log({
    level: "info",
    message: `API available at ${WEBSITE_URL}:${app.get("port")} in ${app.get("env")} mode.`
  });
});

export default server;
