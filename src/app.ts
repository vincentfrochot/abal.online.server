import { Request, Response, Application } from "express";
const { body, validationResult } = require("express-validator");
import express = require("express");
import * as path from "path";
import * as socketio from "socket.io";
import * as bodyParser from "body-parser";
import mongoose = require("mongoose");
import bluebird = require("bluebird");
import logger from "./util/logger";
import {
  MONGODB_URI,
  SOCKET_PORT,
  WEBSITE_URL,
  WEBSITE_PORT,
} from "./util/secrets";

import * as userController from "./api/user";
import * as abaloneController from "./api/abalone";
import { RealtimeServer } from "./socket/RealtimeServer";

const app = express();

// Connect to MongoDB
const mongoUrl = MONGODB_URI || "";
(<any>mongoose).Promise = bluebird;
mongoose
  .connect(mongoUrl, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Connection to MONGODB successful");
  })
  .catch((err) => {
    console.log(err);
    logger.log({
      level: "info",
      message: `MongoDB connection error. Please make sure MongoDB is running. ${err}`,
    });
  });

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ strict: false }));

app.use((req, res, next) => {
  // "http://127.0.0.1:8080"  or http://www.abalone.games
  res.header("Access-Control-Allow-Origin", `${WEBSITE_URL}:${WEBSITE_PORT}`); // provided by .env file.
  res.header("Access-Control-Allow-Credentials", "1");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,POST,OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers"
  );

  next();
});

app.set("port", process.env.SERVER_PORT || 4444);
logger.log({
  level: "info",
  message: `app port SET to: ${process.env.SERVER_PORT || 4444}`,
});

// API routes
app.get("/api/v1/user/list", userController.getList);
app.post(
  "/api/v1/user/login",
  [body("email").isEmail().normalizeEmail(), body("nickname").trim().escape(), body("password").trim().escape()],
  userController.logIn
);
app.get("/api/v1/game/abalone/list", abaloneController.getList);
app.get("/api/v1/game/abalone/match", abaloneController.getMatch);

// special serve for static files from the dist/public client project folder.
app.use("/static", express.static(path.join(__dirname + "/../static")));

// react app is coming with a "bundle.js" file.
app.get("/bundle.js", (req, res) => {
  res.sendFile(path.join(__dirname + "/../bundle.js"));
});

app.get("/1.bundle.js", (req, res) => {
  res.sendFile(path.join(__dirname + "/../1.bundle.js"));
});

// lastly, we try to serve the react App
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname + "/../index.html"));
});

// @TODO: for https, use let's encrypt
const server = require("http").createServer(app); // require("https") instead. Find where to give the certificate
server.listen(SOCKET_PORT, () => {
  logger.log({
    level: "info",
    message: `Socket server available at ${WEBSITE_URL}:${SOCKET_PORT} in ${app.get(
      "env"
    )} mode.`,
  });
});
const io = require("socket.io")(server, { cookie: false });

const realtimeServer = new RealtimeServer(io);
realtimeServer.start();

export default app;
