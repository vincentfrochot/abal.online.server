import logger from "./logger";
import * as dotenv from "dotenv";
import * as fs from "fs";

if (fs.existsSync(".env")) {
  logger.debug("Using .env file to supply config environment variables");
  dotenv.config({ path: ".env" });
} else {
  logger.debug(
    "Using .env.example file to supply config environment variables"
  );
  dotenv.config({ path: ".env.example" }); // you can delete this after you create your own .env file!
}
export const ENVIRONMENT = process.env.NODE_ENV;
const PROD = ENVIRONMENT === "production"; // Anything else is treated as 'dev'

export const WEBSITE_URL = PROD
  ? process.env["WEBSITE_URL"]
  : process.env["WEBSITE_URL_LOCAL"];
if (!WEBSITE_URL) {
  logger.error("No website url. Set WEBSITE_URL environment variable.");
  process.exit(1);
}

export const WEBSITE_PORT = PROD
  ? process.env["WEBSITE_PORT"]
  : process.env["WEBSITE_PORT_LOCAL"];
if (!WEBSITE_PORT) {
  logger.error("No website port. Set WEBSITE_PORT environment variable.");
  process.exit(1);
}

export const SOCKET_PORT = process.env["SOCKET_PORT"];

export const SESSION_SECRET = process.env["SESSION_SECRET"];
export const MONGODB_URI = PROD
  ? process.env["MONGODB_URI"] + "/" + process.env["MONGO_DB_NAME"]
  : process.env["MONGODB_URI_LOCAL"] + "/" + process.env["MONGO_DB_NAME"];

if (!SESSION_SECRET) {
  logger.error("No client secret. Set SESSION_SECRET environment variable.");
  process.exit(1);
}

if (!MONGODB_URI) {
  if (PROD) {
    logger.error(
      "No mongo connection string. Set MONGODB_URI environment variable."
    );
  } else {
    logger.error(
      "No mongo connection string. Set MONGODB_URI_LOCAL environment variable."
    );
  }
  process.exit(1);
}
