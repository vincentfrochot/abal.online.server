# Abal.Online.Server

Abalone Server

https://127.0.0.1:443/api/v1/user/list
https://abal.online:443/api/v1/user/list


## Mongo cheatsheet
use abalone
db.users.find({"nickname": /somestring/})
db.users.update({ _id: ObjectId("5f1afd9ccd37cc309a7087e4") }, { $set: { nickname:"z" } } );
db.users.deleteOne( {"_id": ObjectId("5f4d66980986e02624f39328")});

## IP cheatsheet
sudo iptables -A INPUT -s **<IP>** -j DROP
sudo iptables -D INPUT -s **<IP>** -j DROP
sudo iptables -L INPUT -v -n